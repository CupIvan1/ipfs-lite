package threads.lite;

import static junit.framework.TestCase.assertNotNull;
import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import android.content.Context;

import androidx.test.core.app.ApplicationProvider;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Objects;
import java.util.concurrent.atomic.AtomicBoolean;

import threads.lite.cid.Cid;
import threads.lite.cid.Multiaddr;
import threads.lite.cid.PeerId;
import threads.lite.core.Page;
import threads.lite.core.PeerInfo;
import threads.lite.core.Reservation;
import threads.lite.core.Server;
import threads.lite.core.Session;
import threads.lite.core.TimeoutCancellable;
import threads.lite.quic.Connection;


@RunWith(AndroidJUnit4.class)
public class IpfsLocalTest {

    private static final String TAG = IpfsLocalTest.class.getSimpleName();


    private static Context context;

    @BeforeClass
    public static void setup() {
        context = ApplicationProvider.getApplicationContext();
    }


    @Test
    public void own_service_connect() throws Exception {

        IPFS ipfs = TestEnv.getTestInstance(context);
        Server server = TestEnv.getServer();
        assertNotNull(server);

        try (Session session = ipfs.createSession()) {

            Multiaddr multiaddr = Multiaddr.getLoopbackAddress(ipfs.self(), server.getPort());

            Connection connection = IPFS.dial(session, multiaddr, IPFS.getConnectionParameters());
            assertNotNull(connection);

            PeerInfo info = ipfs.getPeerInfo(connection);
            assertNotNull(info);
            LogUtils.error(TAG, info.toString());


            Page page = IPFS.pull(connection);
            assertNotNull(page);
            assertEquals(page.peerId(), ipfs.self());
            assertNotNull(page.value());
            assertEquals(page.value().length, 0);
            assertNotNull(page.getEolDate());

            String text = "Moin Moin";
            ipfs.setRecordSupplier(() -> {
                try {
                    return ipfs.createSelfSignedIpnsRecord(text.getBytes(), null);
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
            });

            page = IPFS.pull(connection);
            assertNotNull(page);
            assertEquals(page.peerId(), ipfs.self());
            assertNotNull(page.value());
            assertEquals(new String(page.value()), text);
            assertNotNull(page.getEolDate());

        }
        Thread.sleep(3000);
        assertEquals(server.numServerConnections(), 0);
    }

    //@Test
    public void direct_dial_pc() throws Exception {

        IPFS ipfs = TestEnv.getTestInstance(context);


        try (Session session = ipfs.createSession()) {

            String test = new String(TestEnv.getRandomBytes(20));
            Cid cid = IPFS.storeText(session, test);
            assertNotNull(cid);

            Multiaddr multiaddr = IPFS.decodeMultiaddr(
                    "/ip4/192.168.43.171/udp/4001/quic-v1/p2p/12D3KooWJFayop1evwe1A3yLxFR7CwFAENchEvUGU9Sxvmod2aNU");
            Connection connection = IPFS.dial(session, multiaddr, IPFS.getConnectionParameters());
            assertNotNull(connection);


            PeerInfo info = ipfs.getPeerInfo(connection);
            assertNotNull(info);
            LogUtils.error(TAG, info.toString());

        }

    }


    //@Test // not working since it is impossible to put the pc as a static relay, when behind a NAT
    public void pc_static_relay() throws Exception {
        // Precondition: pc is configured as static relay (and running)


        Server server = TestEnv.getServer();
        assertNotNull(server);


        // (1) create a reservation to the pc

        Multiaddr pc = IPFS.decodeMultiaddr("/ip4/192.168.43.172/udp/4001/quic-v1/p2p/12D3KooWSR9wEwGHjd6Rj3CViDPvHX5FGkXop4y6wNVzRvbZSXoK");

        Reservation reservation = IPFS.reservation(server, pc);

        Objects.requireNonNull(reservation);
        assertFalse(reservation.limit().limited());

        Multiaddr circuitMultiaddr = reservation.circuitAddress();
        assertNotNull(circuitMultiaddr);

        // (2) create a dummy and connect to ipfs node multiaddrs
        Dummy dummy = Dummy.getInstance(context);

        try (Session dummySession = dummy.createSession()) {

            Connection connection = dummySession.dial(circuitMultiaddr, IPFS.getConnectionParameters());

            assertNotNull(connection);

            // (3) peer info test
            PeerInfo info = dummy.getHost().getPeerInfo(connection);
            assertNotNull(info);

            LogUtils.error(TAG, info.toString());

            // (4) add much more tests


            connection.close();
        }


    }

    //@Test
    public void direct_dial_via_relay_pc() throws Exception {

        IPFS ipfs = TestEnv.getTestInstance(context);
        Server server = TestEnv.getServer();
        Objects.requireNonNull(server);

        for (Multiaddr addr : Multiaddr.getSiteLocalAddresses(ipfs.self(), server.getPort())) {
            LogUtils.error(TAG, "./ipfs swarm connect " + addr);
        }

        try (Session session = ipfs.createSession()) {
            Cid cid = IPFS.storeData(session, TestEnv.getRandomBytes(500000));
            LogUtils.error(TAG, "./ipfs get " + cid);
        }


        Thread.sleep(500000);

        try (Session session = ipfs.createSession()) {

            Multiaddr multiaddr = IPFS.decodeMultiaddr("/ip6/2604:1380:45d1:3c00::13/udp/4001/quic-v1/p2p/12D3KooW9swwwtAj83nNXZjvzNUvV1pqEqoY7MMr8ZCU5yRaSbXQ/p2p-circuit/p2p/12D3KooWSR9wEwGHjd6Rj3CViDPvHX5FGkXop4y6wNVzRvbZSXoK");
            Connection connection = IPFS.dial(session, multiaddr, IPFS.getConnectionParameters());
            assertNotNull(connection);

            PeerInfo info = ipfs.getPeerInfo(connection);
            assertNotNull(info);
            LogUtils.error(TAG, info.toString());
        }
    }

    //@Test
    public void find_pc_data() throws Exception {

        IPFS ipfs = TestEnv.getTestInstance(context);

        try (Session session = ipfs.createSession()) {

            Cid cid = Cid.decode("QmdjoDyD7JC8K3JnYqtEE31BoZC7yxa5pzJbWZfFB41gcF");

            byte[] data = IPFS.getData(session, cid, new TimeoutCancellable(60));
            assertNotNull(data);

        }
    }


    //@Test
    public void find_pc() throws Exception {

        IPFS ipfs = TestEnv.getTestInstance(context);

        try (Session session = ipfs.createSession()) {
            PeerId pc = PeerId.decode("12D3KooWSR9wEwGHjd6Rj3CViDPvHX5FGkXop4y6wNVzRvbZSXoK");
            AtomicBoolean connect = new AtomicBoolean(false);
            IPFS.findPeer(session, pc,
                    multiaddr -> {
                        LogUtils.error(TAG, multiaddr.toString());

                        try {
                            Connection conn = IPFS.dial(
                                    session, multiaddr, IPFS.getConnectionParameters());
                            assertNotNull(conn);
                            connect.set(true);
                        } catch (Throwable throwable) {
                            LogUtils.error(TAG, throwable);
                        }


                    }, connect::get);

            assertTrue(connect.get());

        }
    }
}
