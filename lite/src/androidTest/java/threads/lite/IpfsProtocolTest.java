package threads.lite;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import android.content.Context;

import androidx.test.core.app.ApplicationProvider;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import junit.framework.TestCase;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Map;
import java.util.Objects;

import threads.lite.cid.Multiaddr;
import threads.lite.core.PeerInfo;
import threads.lite.core.ProtocolHandler;
import threads.lite.core.Server;
import threads.lite.core.Session;
import threads.lite.quic.Connection;

@RunWith(AndroidJUnit4.class)
public class IpfsProtocolTest {

    private static Context context;

    @BeforeClass
    public static void setup() {
        context = ApplicationProvider.getApplicationContext();
    }


    @Test
    public void protocol_test() throws Exception {
        IPFS ipfs = TestEnv.getTestInstance(context);
        Server server = TestEnv.getServer();
        TestCase.assertNotNull(server);

        assertEquals(server.numServerConnections(), 0);


        Map<String, ProtocolHandler> serverProtocols = server.getProtocols();
        assertNotNull(serverProtocols);


        assertTrue(serverProtocols.containsKey(IPFS.LITE_PUSH_PROTOCOL));
        assertTrue(serverProtocols.containsKey(IPFS.LITE_PULL_PROTOCOL));
        assertTrue(serverProtocols.containsKey(IPFS.LITE_SWAP_PROTOCOL));
        assertTrue(serverProtocols.containsKey(IPFS.MULTISTREAM_PROTOCOL));
        assertTrue(serverProtocols.containsKey(IPFS.IDENTITY_PROTOCOL));
        assertTrue(serverProtocols.containsKey(IPFS.IDENTITY_PUSH_PROTOCOL));
        assertTrue(serverProtocols.containsKey(IPFS.BITSWAP_PROTOCOL));
        assertTrue(serverProtocols.containsKey(IPFS.HOLE_PUNCH_PROTOCOL));
        assertTrue(serverProtocols.containsKey(IPFS.RELAY_PROTOCOL_STOP));
        assertEquals(serverProtocols.size(), 9);

        try (Session session = ipfs.createSession()) {


            Map<String, ProtocolHandler> protocols = session.getProtocols();
            assertNotNull(protocols);


            assertTrue(protocols.containsKey(IPFS.LITE_PUSH_PROTOCOL));
            assertTrue(protocols.containsKey(IPFS.LITE_PULL_PROTOCOL));
            assertTrue(protocols.containsKey(IPFS.LITE_SWAP_PROTOCOL));
            assertTrue(protocols.containsKey(IPFS.MULTISTREAM_PROTOCOL));
            assertTrue(protocols.containsKey(IPFS.IDENTITY_PROTOCOL));
            assertTrue(protocols.containsKey(IPFS.IDENTITY_PUSH_PROTOCOL));
            assertTrue(protocols.containsKey(IPFS.BITSWAP_PROTOCOL));
            assertEquals(protocols.size(), 7);


            // now you have added the protocol to a session and server server session


            // for testing we are connecting to our own server
            Multiaddr ownLocalServerAddress = Multiaddr.getLoopbackAddress(
                    ipfs.self(), server.getPort());
            Objects.requireNonNull(ownLocalServerAddress);

            Connection connection = IPFS.dial(session, ownLocalServerAddress,
                    IPFS.getConnectionParameters());
            assertNotNull(connection);

            PeerInfo info = ipfs.getPeerInfo(connection);
            TestCase.assertNotNull(info);


            assertEquals(serverProtocols.size(), info.protocols().length);


        }

        Thread.sleep(3000);
        assertEquals(server.numServerConnections(), 0);
    }

}
