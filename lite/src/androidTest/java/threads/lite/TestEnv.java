package threads.lite;

import static org.junit.Assert.assertNotNull;

import android.content.Context;
import android.net.ConnectivityManager;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.io.File;
import java.io.IOException;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.locks.ReentrantLock;

import threads.lite.cid.Multiaddr;
import threads.lite.cid.Network;
import threads.lite.core.AutonatResult;
import threads.lite.core.NatType;
import threads.lite.core.Reservation;
import threads.lite.core.Server;
import threads.lite.core.TimeoutCancellable;
import threads.lite.quic.Connection;
import threads.lite.quic.StreamRequester;

final class TestEnv {

    private static final String TAG = TestEnv.class.getSimpleName();

    @NonNull
    private static final AtomicReference<Server> SERVER = new AtomicReference<>();
    @NonNull
    private static final ReentrantLock reserve = new ReentrantLock();


    @NonNull
    static File createCacheFile(Context context) throws IOException {
        return File.createTempFile("temp", ".cid", context.getCacheDir());
    }

    public static byte[] getRandomBytes(int number) {
        byte[] bytes = new byte[number];
        new Random().nextBytes(bytes);
        return bytes;
    }

    @Nullable
    public static Server getServer() {
        return SERVER.get();
    }

    public static IPFS getTestInstance(@NonNull Context context) throws Exception {
        reserve.lock();
        try {
            IPFS ipfs = IPFS.getInstance(context);

            ipfs.getBlockStore().clear(); // clears the default blockStore
            ipfs.getPageStore().clear(); // clear the page store

            if (SERVER.get() == null) {

                Server server = ipfs.startServer(5001,
                        connection -> LogUtils.debug(TAG, "Incoming connection : "
                                + connection.remoteAddress()),
                        connection -> LogUtils.debug(TAG, "Closing connection : "
                                + connection.remoteAddress()),
                        peerId -> {
                            LogUtils.debug(TAG, "Peer Gated : " + peerId.toString());
                            return false;
                        });

                // just to get coverage of shutdown method
                Runtime.getRuntime().addShutdownHook(new Thread(server::shutdown));
                SERVER.set(server);

                try {
                    ConnectivityManager connectivityManager = (ConnectivityManager)
                            context.getSystemService(Context.CONNECTIVITY_SERVICE);

                    connectivityManager.registerDefaultNetworkCallback(new NetworkCallback(
                            ipfs, server));
                } catch (Throwable throwable) {
                    LogUtils.error(TAG, throwable);
                }

                if (Network.isNetworkConnected(context)) {
                    AutonatResult result = IPFS.autonat(server);
                    LogUtils.error(TAG, "Autonat : " + result);

                    LogUtils.error(TAG, "Nat Type " + result.natType());
                    LogUtils.error(TAG, "Success " + result.success());
                    LogUtils.error(TAG, "Address " + result.dialableAddress());

                    if (result.success()) {

                        IPFS.swarm(server, new TimeoutCancellable(120));

                        Set<Connection> swarm = server.swarm();
                        for (Connection connection : swarm) {
                            LogUtils.error(TAG, "Swarm Address " +
                                    StreamRequester.remoteMultiaddr(connection));
                        }

                    } else {

                        if (server.getNatType() == NatType.RESTRICTED_CONE) {
                            Set<Reservation> reservations = IPFS.reservations(
                                    server, new TimeoutCancellable(120));

                            for (Reservation reservation : reservations) {
                                LogUtils.error(TAG, reservation.toString());
                            }

                            LogUtils.error(TAG, "Next Reservation Cycle " +
                                    IPFS.nextReservationCycle(server) + " [min]");

                        }

                        assertNotNull(server.getSocket());

                    }

                    Set<Multiaddr> set = server.dialableAddresses();
                    for (Multiaddr addr : set) {
                        LogUtils.warning(TAG, "Dialable Address " + addr.toString());
                    }
                }
            }

            return ipfs;
        } finally {
            reserve.unlock();
        }
    }

    private static class NetworkCallback extends ConnectivityManager.NetworkCallback {
        private final IPFS ipfs;
        private final Server server;

        private NetworkCallback(IPFS ipfs, Server server) {
            this.ipfs = ipfs;
            this.server = server;
        }

        @Override
        public void onAvailable(@NonNull android.net.Network network) {
            try {
                ipfs.updateNetwork();
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }
        }

        @Override
        public void onLost(@NonNull android.net.Network network) {
            try {
                ipfs.updateNetwork();
                server.updateNetwork();
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }
        }
    }

}
