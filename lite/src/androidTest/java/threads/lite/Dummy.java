package threads.lite;

import android.content.Context;

import androidx.annotation.NonNull;

import java.util.Collections;
import java.util.Date;
import java.util.List;

import threads.lite.cid.Cid;
import threads.lite.cid.Multiaddr;
import threads.lite.cid.Peer;
import threads.lite.cid.PeerId;
import threads.lite.core.BlockStore;
import threads.lite.core.Page;
import threads.lite.core.Session;
import threads.lite.crypto.Key;
import threads.lite.host.LiteHost;

public final class Dummy {

    @NonNull
    private final LiteHost host;
    @NonNull
    private final BlockStoreCache blockStore;

    private Dummy(@NonNull Context context) throws Exception {
        this.blockStore = BlockStoreCache.createInstance(context);
        this.host = new LiteHost(Key.generateKeys(), blockStore,
                new PeerStore(), new SwarmStore(), new PageStore());
    }

    @NonNull
    public static Dummy getInstance(@NonNull Context context) throws Exception {
        return new Dummy(context);
    }

    @NonNull
    public PeerId self() {
        return host.self();
    }


    void clearDatabase() {
        blockStore.close();
    }

    @NonNull
    LiteHost getHost() {
        return host;
    }

    @NonNull
    public Session createSession() {
        return host.createSession(blockStore, cid -> false);
    }

    @NonNull
    public Session createSession(@NonNull BlockStore blockStore) {
        return host.createSession(blockStore, cid -> false);
    }

    private static class PeerStore implements threads.lite.core.PeerStore {
        @Override
        public List<Peer> getRandomPeers(int limit) {
            return Collections.emptyList();
        }


        @Override
        public void storePeer(@NonNull Peer peer) {

        }

        @Override
        public void removePeer(Peer peer) {

        }
    }

    private static class SwarmStore implements threads.lite.core.SwarmStore {
        @Override
        public void storeMultiaddr(@NonNull Multiaddr multiaddr) {

        }

        @Override
        public void removeMultiaddr(@NonNull Multiaddr multiaddr) {

        }

        @Override
        public List<Multiaddr> getMultiaddrs() {
            return Collections.emptyList();
        }
    }

    private static class PageStore implements threads.lite.core.PageStore {
        @Override
        public void storePage(@NonNull Page page) {

        }

        @Override
        public Page getPage(@NonNull PeerId peerId) {
            return null;
        }

        @Override
        public void updatePageContent(@NonNull PeerId peerId, @NonNull Cid cid, @NonNull Date eol) {

        }

        @Override
        public void clear() {

        }

        @Override
        public Cid getPageContent(@NonNull PeerId peerId) {
            return null;
        }
    }
}
