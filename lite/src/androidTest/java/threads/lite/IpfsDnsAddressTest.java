package threads.lite;


import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import android.content.Context;

import androidx.test.core.app.ApplicationProvider;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import junit.framework.TestCase;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.net.UnknownHostException;
import java.util.Set;

import threads.lite.cid.Cid;
import threads.lite.cid.Multiaddr;
import threads.lite.cid.Network;

@RunWith(AndroidJUnit4.class)
public class IpfsDnsAddressTest {
    private static final String TAG = IpfsDnsAddressTest.class.getSimpleName();
    private static Context context;

    @BeforeClass
    public static void setup() {
        context = ApplicationProvider.getApplicationContext();
    }

    @Test
    public void test_dnsLink() throws Exception {

        if (!Network.isNetworkConnected(context)) {
            LogUtils.warning(TAG, "nothing to test here NO NETWORK");
            return;
        }
        String link = IPFS.resolveDnsLink("blog.ipfs.io");
        TestCase.assertNotNull(link);
        TestCase.assertFalse(link.isEmpty());
        Cid cid = Cid.decode(link.replace(IPFS.IPFS_PATH, ""));
        assertNotNull(cid);
    }

    @Test
    public void test_dnsAddress() throws Exception {
        IPFS ipfs = TestEnv.getTestInstance(context);

        if (!Network.isNetworkConnected(context)) {
            LogUtils.warning(TAG, "nothing to test here NO NETWORK");
            return;
        }
        String address = "/dnsaddr/bootstrap.libp2p.io/p2p/QmNnooDu7bfjPFoTZYxMNLWUQJyrVwtbZg5gBMjTezGAJN";
        Multiaddr dnsaddr = Multiaddr.create(address);
        assertEquals(dnsaddr.toString(), address);

        // check function
        Multiaddr check = Multiaddr.create(dnsaddr.toString());
        assertArrayEquals(check.address(), dnsaddr.address());
        assertEquals(check.peerId(), dnsaddr.peerId());
        assertEquals(check.toString(), dnsaddr.toString());
        assertEquals(check, dnsaddr);


        Set<Multiaddr> addresses = ipfs.resolveDnsaddr(dnsaddr);
        assertNotNull(addresses);
        assertFalse(addresses.isEmpty());
    }

    @Test
    public void test_dnsAddress2() throws Exception {
        IPFS ipfs = TestEnv.getTestInstance(context);

        String address = "/dnsaddr/ipfs.jamonbread.tech/p2p/12D3KooWHnwgjLJjDgBVBxFJXGJYzrwM9RrsYi9KqJYDNoaGYBhC";
        Multiaddr dnsaddr = Multiaddr.create(address);
        assertEquals(dnsaddr.toString(), address);

        // check function
        Multiaddr check = Multiaddr.create(dnsaddr.toString());
        assertArrayEquals(check.address(), dnsaddr.address());
        assertEquals(check.peerId(), dnsaddr.peerId());
        assertEquals(check.toString(), dnsaddr.toString());
        assertEquals(check, dnsaddr);

        Set<Multiaddr> addresses = ipfs.resolveDnsaddr(dnsaddr);
        assertNotNull(addresses);
        assertTrue(addresses.isEmpty());
    }

    @Test
    public void test_dns() throws Exception {

        if (!Network.isNetworkConnected(context)) {
            LogUtils.warning(TAG, "nothing to test here NO NETWORK");
            return;
        }
        // not working example (reason might be that the "quicweb3-storage-am6.web3.dwebops.net"
        // is not known by google dns resolver)
        // must use the dns server which was evaluated by dnsaddr
        // -> but still good for code coverage
        String address = "/dns4/quicweb3-storage-am6.web3.dwebops.net/udp/4001/quic-v1/p2p/12D3KooWPySxxWQjBgX9Jp6uAHQfVmdq8HG1gVvS1fRawHNSrmqW";
        Multiaddr dnsaddr = Multiaddr.create(address);
        assertEquals(dnsaddr.toString(), address);

        // check function
        Multiaddr check = Multiaddr.create(dnsaddr.toString());
        assertArrayEquals(check.address(), dnsaddr.address());
        assertEquals(check.peerId(), dnsaddr.peerId());
        assertEquals(check.toString(), dnsaddr.toString());
        assertEquals(check, dnsaddr);

        try {
            check.inetAddress(); // this will fail
            fail();
        } catch (UnknownHostException unknownHostException) {
            // expected exception
        }


        address = "/dns4/luflosi.de/udp/4002/quic-v1/p2p/12D3KooWBqQrnTqx9Wp89p2bD1hrwmXYJQ5x1fDfigRCfZJGKQfr/p2p-circuit/p2p/12D3KooWKQTnJ3vkV3aQFKiZQKJTfqrzj75Gfpp6CLMdBvqpu6Ac";
        dnsaddr = Multiaddr.create(address);
        assertEquals(dnsaddr.toString(), address);
        assertEquals(dnsaddr.getRelayId().toString(), "12D3KooWBqQrnTqx9Wp89p2bD1hrwmXYJQ5x1fDfigRCfZJGKQfr");

        // check function
        check = Multiaddr.create(dnsaddr.toString());
        assertArrayEquals(check.address(), dnsaddr.address());
        assertEquals(check.peerId(), dnsaddr.peerId());
        assertEquals(check.toString(), dnsaddr.toString());
        assertEquals(check, dnsaddr);

    }
}
