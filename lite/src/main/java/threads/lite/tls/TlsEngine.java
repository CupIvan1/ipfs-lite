/*
 * Copyright © 2020, 2021, 2022 Peter Doornbosch
 *
 * This file is part of Agent15, an implementation of TLS 1.3 in Java.
 *
 * Agent15 is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Agent15 is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package threads.lite.tls;

import static threads.lite.tls.NamedGroup.secp256r1;
import static threads.lite.tls.NamedGroup.secp384r1;
import static threads.lite.tls.NamedGroup.secp521r1;
import static threads.lite.tls.NamedGroup.x25519;
import static threads.lite.tls.NamedGroup.x448;
import static threads.lite.tls.SignatureScheme.ecdsa_secp256r1_sha256;
import static threads.lite.tls.SignatureScheme.rsa_pss_rsae_sha256;
import static threads.lite.tls.SignatureScheme.rsa_pss_rsae_sha384;
import static threads.lite.tls.SignatureScheme.rsa_pss_rsae_sha512;

import android.os.Build;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.security.SignatureException;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.security.spec.ECGenParameterSpec;
import java.security.spec.NamedParameterSpec;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

public abstract class TlsEngine implements MessageProcessor, TrafficSecrets {
    private final X509TrustManager customTrustManager;
    PublicKey publicKey;
    PrivateKey privateKey;
    TlsState state;
    X509Certificate remoteCertificate;
    X509Certificate[] remoteCertificateChain;

    TlsEngine(X509TrustManager customTrustManager) {
        this.customTrustManager = customTrustManager;
    }


    /**
     * Compute the signature used in certificate verify message to proof possession of private key.
     *
     * @param content               the content to be signed (transcript hash)
     * @param certificatePrivateKey the private key associated with the certificate
     * @param client                whether the signature must be computed
     */
    static byte[] computeSignature(byte[] content, PrivateKey certificatePrivateKey,
                                   SignatureScheme signatureScheme, boolean client) throws ErrorAlert {
        // https://tools.ietf.org/html/rfc8446#section-4.4.3

        //   The digital signature is then computed over the concatenation of:
        //   -  A string that consists of octet 32 (0x20) repeated 64 times
        //   -  The context string
        //   -  A single 0 byte which serves as the separator
        //   -  The content to be signed"
        try (ByteArrayOutputStream signatureInput = new ByteArrayOutputStream()) {
            signatureInput.write(new String(new byte[]{0x20}).repeat(64).getBytes(StandardCharsets.US_ASCII));
            String contextString = "TLS 1.3, " + (client ? "client" : "server") + " CertificateVerify";
            signatureInput.write(contextString.getBytes(StandardCharsets.US_ASCII));
            signatureInput.write(0x00);
            signatureInput.write(content);


            Signature signatureAlgorithm = getSignatureAlgorithm(signatureScheme);
            signatureAlgorithm.initSign(certificatePrivateKey);
            signatureAlgorithm.update(signatureInput.toByteArray());
            return signatureAlgorithm.sign();

        } catch (IOException | SignatureException e) {
            // sign() throws SignatureException: if this signature object is not initialized properly or if this
            //                                   signature algorithm is unable to process the input data provided.
            throw new RuntimeException(e);
        } catch (InvalidKeyException e) {
            throw new InternalErrorAlert("invalid private key");
        }
    }

    @SuppressWarnings("BooleanMethodIsAlwaysInverted")
    static boolean verifySignature(byte[] signatureToVerify, SignatureScheme signatureScheme,
                                   Certificate certificate, byte[] transcriptHash,
                                   boolean client) throws HandshakeFailureAlert, DecryptErrorAlert {
        // https://tools.ietf.org/html/rfc8446#section-4.4.3
        // "The digital signature is then computed over the concatenation of:
        //   -  A string that consists of octet 32 (0x20) repeated 64 times
        //   -  The context string
        //   -  A single 0 byte which serves as the separator
        //   -  The content to be signed"
        String contextString = "TLS 1.3, " + (client ? "client" : "server") + " CertificateVerify";
        ByteBuffer contentToSign = ByteBuffer.allocate(64 + contextString.getBytes(
                StandardCharsets.ISO_8859_1).length + 1 + transcriptHash.length);
        for (int i = 0; i < 64; i++) {
            contentToSign.put((byte) 0x20);
        }
        // "The context string for a server signature is
        //   "TLS 1.3, server CertificateVerify". "
        contentToSign.put(contextString.getBytes(StandardCharsets.ISO_8859_1));
        contentToSign.put((byte) 0x00);
        // "The content that is covered
        //   under the signature is the hash output as described in Section 4.4.1,
        //   namely:
        //      Transcript-Hash(Handshake Context, Certificate)"
        contentToSign.put(transcriptHash);

        try {
            Signature signatureAlgorithm = getSignatureAlgorithm(signatureScheme);
            signatureAlgorithm.initVerify(certificate);
            signatureAlgorithm.update(contentToSign.array());
            return signatureAlgorithm.verify(signatureToVerify);
        } catch (InvalidKeyException | SignatureException e) {
            throw new DecryptErrorAlert(e.getMessage());
        }

    }

    private static Signature getSignatureAlgorithm(SignatureScheme signatureScheme) throws HandshakeFailureAlert {
        Signature signatureAlgorithm;
        // https://tools.ietf.org/html/rfc8446#section-9.1
        // "A TLS-compliant application MUST support digital signatures with rsa_pkcs1_sha256 (for certificates),
        // rsa_pss_rsae_sha256 (for CertificateVerify and certificates), and ecdsa_secp256r1_sha256."
        if (signatureScheme == rsa_pss_rsae_sha256) {
            try {
                signatureAlgorithm = Signature.getInstance("SHA256withRSA/PSS");
            } catch (NoSuchAlgorithmException e) {
                throw new RuntimeException("Missing RSASSA-PSS support");
            }
        } else if (signatureScheme == rsa_pss_rsae_sha384) {
            try {
                signatureAlgorithm = Signature.getInstance("SHA384withRSA/PSS");
            } catch (NoSuchAlgorithmException e) {
                throw new RuntimeException("Missing RSASSA-PSS support");
            }
        } else if (signatureScheme == rsa_pss_rsae_sha512) {
            try {
                signatureAlgorithm = Signature.getInstance("SHA512withRSA/PSS");
            } catch (NoSuchAlgorithmException e) {
                throw new RuntimeException("Missing RSASSA-PSS support");
            }
        } else if (signatureScheme == ecdsa_secp256r1_sha256) {
            try {
                signatureAlgorithm = Signature.getInstance("SHA256withECDSA");
            } catch (NoSuchAlgorithmException e) {
                throw new RuntimeException("Missing SHA256withECDSA support");
            }
        } else {
            // Bad luck, not (yet) supported.
            throw new HandshakeFailureAlert("Signature algorithm not supported " + signatureScheme);
        }
        return signatureAlgorithm;
    }


    public X509Certificate getRemoteCertificate() {
        return remoteCertificate;
    }

    void checkCertificateValidity(X509Certificate[] certificates, boolean serverTrust) throws BadCertificateAlert {
        try {
            if (customTrustManager != null) {
                if (serverTrust) {
                    customTrustManager.checkServerTrusted(certificates, "RSA");
                } else {
                    customTrustManager.checkClientTrusted(certificates, "RSA");
                }
            } else {
                // https://docs.oracle.com/en/java/javase/11/docs/specs/security/standard-names.html#trustmanagerfactory-algorithms
                // "...that validate certificate chains according to the rules defined by the IETF PKIX working group in RFC 5280 or its successor"
                TrustManagerFactory trustManagerFactory = TrustManagerFactory.getInstance("PKIX");
                trustManagerFactory.init((KeyStore) null);
                X509TrustManager trustMgr = (X509TrustManager) trustManagerFactory.getTrustManagers()[0];
                if (serverTrust) {
                    trustMgr.checkServerTrusted(certificates, "UNKNOWN");
                } else {
                    trustMgr.checkClientTrusted(certificates, "UNKNOWN");
                }
                // If it gets here, the certificates are ok.
            }
        } catch (Throwable throwable) {
            String msg = throwable.getMessage();
            if (msg == null || msg.isBlank()) {
                msg = "certificate validation failed";
            }
            throw new BadCertificateAlert(msg);
        }
    }

    void generateKeys(NamedGroup namedGroup) {
        try {
            KeyPairGenerator keyPairGenerator;
            if (namedGroup == secp256r1 || namedGroup == secp384r1 || namedGroup == secp521r1) {
                keyPairGenerator = KeyPairGenerator.getInstance("EC");
                keyPairGenerator.initialize(new ECGenParameterSpec(namedGroup.toString()));
            } else if (namedGroup == x25519 || namedGroup == x448) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
                    keyPairGenerator = KeyPairGenerator.getInstance("XDH");
                    NamedParameterSpec paramSpec;  // x25519 => X25519
                    paramSpec = new NamedParameterSpec(namedGroup.toString().toUpperCase());
                    keyPairGenerator.initialize(paramSpec);
                } else {
                    throw new RuntimeException("unsupported group " + namedGroup);
                }
            } else {
                throw new RuntimeException("unsupported group " + namedGroup);
            }

            KeyPair keyPair = keyPairGenerator.genKeyPair();
            privateKey = keyPair.getPrivate();
            publicKey = keyPair.getPublic();
        } catch (NoSuchAlgorithmException e) {
            // Invalid runtime
            throw new RuntimeException("missing key pair generator algorithm EC");
        } catch (InvalidAlgorithmParameterException e) {
            // Impossible, would be programming error
            throw new RuntimeException(e);
        }
    }

    // https://tools.ietf.org/html/rfc8446#section-4.4.4
    byte[] computeFinishedVerifyData(byte[] transcriptHash, byte[] baseKey) throws BadRecordMacAlert {
        short hashLength = TlsState.getHashLength();
        byte[] finishedKey = state.hkdfExpandLabel(baseKey, "finished", "", hashLength);
        String macAlgorithmName = "HmacSHA" + (hashLength * 8);
        SecretKeySpec hmacKey = new SecretKeySpec(finishedKey, macAlgorithmName);

        try {
            Mac hmacAlgorithm = Mac.getInstance(macAlgorithmName);
            hmacAlgorithm.init(hmacKey);
            hmacAlgorithm.update(transcriptHash);
            return hmacAlgorithm.doFinal();
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException("Missing " + macAlgorithmName + " support");
        } catch (InvalidKeyException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public byte[] getClientHandshakeTrafficSecret() {
        if (state != null) {
            return state.getClientHandshakeTrafficSecret();
        } else {
            throw new IllegalStateException("Traffic secret not yet available");
        }
    }

    @Override
    public byte[] getServerHandshakeTrafficSecret() {
        if (state != null) {
            return state.getServerHandshakeTrafficSecret();
        } else {
            throw new IllegalStateException("Traffic secret not yet available");
        }
    }

    @Override
    public byte[] getClientApplicationTrafficSecret() {
        if (state != null) {
            return state.getClientApplicationTrafficSecret();
        } else {
            throw new IllegalStateException("Traffic secret not yet available");
        }
    }

    @Override
    public byte[] getServerApplicationTrafficSecret() {
        if (state != null) {
            return state.getServerApplicationTrafficSecret();
        } else {
            throw new IllegalStateException("Traffic secret not yet available");
        }
    }

    enum Status {
        Initial,
        ClientHelloSent,
        ServerHelloReceived,
        EncryptedExtensionsReceived,
        CertificateRequestReceived,
        CertificateReceived,
        CertificateVerifyReceived,
        Finished,
        ClientHelloReceived,
        ServerHelloSent,
        EncryptedExtensionsSent,
        CertificateRequestSent,
        CertificateSent,
        CertificateVerifySent,
        FinishedSent,
        FinishedReceived
    }
}

