/*
 * Copyright © 2019, 2020, 2021, 2022 Peter Doornbosch
 *
 * This file is part of Agent15, an implementation of TLS 1.3 in Java.
 *
 * Agent15 is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Agent15 is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package threads.lite.tls;

import android.os.Build;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.interfaces.ECPublicKey;
import java.security.interfaces.XECPublicKey;

import javax.crypto.KeyAgreement;
import javax.crypto.Mac;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import threads.lite.Utils;

public final class TlsState {
    private static final Charset ISO_8859_1 = StandardCharsets.ISO_8859_1;

    private static final String labelPrefix = "tls13 ";
    // Assuming AES-128, use 32 for AES-256
    private static final short hashLength = 32;  // Assuming SHA-256, use 48 for SHA-384
    //private final HKDF hkdf;
    private final byte[] emptyHash;
    private final byte[] psk;
    private final TranscriptHash transcriptHash;
    private final Mac mac;
    private boolean pskSelected;
    private PublicKey serverSharedKey;
    private PrivateKey clientPrivateKey;
    private byte[] earlySecret;
    private byte[] resumptionMasterSecret;
    private byte[] serverHandshakeTrafficSecret;
    private byte[] clientHandshakeTrafficSecret;
    private byte[] handshakeSecret;
    private byte[] clientApplicationTrafficSecret;
    private byte[] serverApplicationTrafficSecret;
    private byte[] sharedSecret;
    private byte[] masterSecret;

    private TlsState(TranscriptHash transcriptHash, byte[] psk) throws BadRecordMacAlert {
        this.psk = psk;
        this.transcriptHash = transcriptHash;

        // https://tools.ietf.org/html/rfc8446#section-7.1
        // "The Hash function used by Transcript-Hash and HKDF is the cipher suite hash algorithm."
        String hashAlgorithm = "SHA-" + (hashLength * 8);
        MessageDigest hashFunction;
        try {
            hashFunction = MessageDigest.getInstance(hashAlgorithm);
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
        String macAlgorithm = "HmacSHA" + (hashLength * 8);
        try {
            mac = Mac.getInstance(macAlgorithm);
        } catch (Throwable throwable) {
            throw new IllegalStateException(throwable);
        }

        emptyHash = hashFunction.digest(Utils.BYTES_EMPTY);

        if (psk == null) {
            // https://tools.ietf.org/html/rfc8446#section-7.1
            // "If a given secret is not available, then the 0-value consisting of a
            //   string of Hash.length bytes set to zeros is used."
            psk = new byte[hashLength];
        }
        computeEarlySecret(psk);
    }

    public TlsState(TranscriptHash transcriptHash) throws BadRecordMacAlert {
        this(transcriptHash, null);
    }

    public static short getHashLength() {
        return hashLength;
    }


    public static byte[] expandHmac(Mac mac, byte[] secret, byte[] info, int length) throws BadRecordMacAlert {
        try {
            SecretKey secretKey = new SecretKeySpec(secret, mac.getAlgorithm());
            mac.init(secretKey);

            if (info == null) {
                info = Utils.BYTES_EMPTY;
            }

               /*
                The output OKM is calculated as follows:
                  N = ceil(L/HashLen)
                  T = T(1) | T(2) | T(3) | ... | T(N)
                  OKM = first L bytes of T
                where:
                  T(0) = empty string (zero length)
                  T(1) = HMAC-Hash(PRK, T(0) | info | 0x01)
                  T(2) = HMAC-Hash(PRK, T(1) | info | 0x02)
                  T(3) = HMAC-Hash(PRK, T(2) | info | 0x03)
                  ...
                 */

            byte[] blockN = Utils.BYTES_EMPTY;

            int iterations = (int) Math.ceil(((double) length) / ((double) mac.getMacLength()));

            if (iterations > 255) {
                throw new IllegalArgumentException("out length must be maximal 255 * hash-length; requested: " + length + " bytes");
            }

            ByteBuffer buffer = ByteBuffer.allocate(length);
            int remainingBytes = length;
            int stepSize;

            for (int i = 0; i < iterations; i++) {
                mac.update(blockN);
                mac.update(info);
                mac.update((byte) (i + 1));

                blockN = mac.doFinal();

                stepSize = Math.min(remainingBytes, blockN.length);

                buffer.put(blockN, 0, stepSize);
                remainingBytes -= stepSize;
            }

            return buffer.array();


        } catch (Throwable throwable) {
            throw new BadRecordMacAlert(throwable.getMessage());
        }
    }

    private static byte[] extractHmac(Mac mac, byte[] salt, byte[] info) throws BadRecordMacAlert {
        try {
            SecretKey secretKey = new SecretKeySpec(salt, mac.getAlgorithm());
            mac.init(secretKey);
            if (info == null || info.length == 0) {
                throw new IllegalArgumentException("provided info must be at least of size 1 and not null");
            }
            return mac.doFinal(info);
        } catch (Throwable throwable) {
            throw new BadRecordMacAlert(throwable.getMessage());
        }
    }

    private void computeEarlySecret(byte[] ikm) throws BadRecordMacAlert {
        byte[] zeroSalt = new byte[hashLength];
        earlySecret = extractHmac(mac, zeroSalt, ikm);
    }

    public void computeSharedSecret() {
        try {
            KeyAgreement keyAgreement;
            if (serverSharedKey instanceof ECPublicKey) {
                keyAgreement = KeyAgreement.getInstance("ECDH");
            } else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
                    if (serverSharedKey instanceof XECPublicKey) {
                        keyAgreement = KeyAgreement.getInstance("XDH");
                    } else {
                        throw new RuntimeException("Unsupported key type");
                    }
                } else {
                    throw new RuntimeException("Unsupported key type");
                }
            }

            keyAgreement.init(clientPrivateKey);
            keyAgreement.doPhase(serverSharedKey, true);

            sharedSecret = keyAgreement.generateSecret();
        } catch (NoSuchAlgorithmException | InvalidKeyException e) {
            throw new RuntimeException("Unsupported crypto: " + e);
        }
    }

    public void computeEarlyTrafficSecret() throws BadRecordMacAlert {
        byte[] clientHelloHash = transcriptHash.getHash(HandshakeType.client_hello);

        hkdfExpandLabel(earlySecret, "c e traffic", clientHelloHash, hashLength);
    }

    public void computeHandshakeSecrets() throws BadRecordMacAlert {
        byte[] derivedSecret = hkdfExpandLabel(earlySecret, "derived", emptyHash, hashLength);

        handshakeSecret = extractHmac(mac, derivedSecret, sharedSecret);

        byte[] handshakeHash = transcriptHash.getHash(HandshakeType.server_hello);

        clientHandshakeTrafficSecret = hkdfExpandLabel(handshakeSecret, "c hs traffic", handshakeHash, hashLength);

        serverHandshakeTrafficSecret = hkdfExpandLabel(handshakeSecret, "s hs traffic", handshakeHash, hashLength);


    }

    public void computeApplicationSecrets() throws BadRecordMacAlert {
        computeApplicationSecrets(handshakeSecret);
    }

    private void computeApplicationSecrets(byte[] handshakeSecret) throws BadRecordMacAlert {
        byte[] serverFinishedHash = transcriptHash.getServerHash(HandshakeType.finished);

        byte[] derivedSecret = hkdfExpandLabel(handshakeSecret, "derived", emptyHash, hashLength);
        byte[] zeroKey = new byte[hashLength];
        masterSecret = extractHmac(mac, derivedSecret, zeroKey);
        clientApplicationTrafficSecret = hkdfExpandLabel(masterSecret, "c ap traffic",
                serverFinishedHash, hashLength);
        serverApplicationTrafficSecret = hkdfExpandLabel(masterSecret, "s ap traffic",
                serverFinishedHash, hashLength);
    }

    public void computeResumptionMasterSecret() throws BadRecordMacAlert {
        byte[] clientFinishedHash = transcriptHash.getClientHash(HandshakeType.finished);

        resumptionMasterSecret = hkdfExpandLabel(masterSecret, "res master",
                clientFinishedHash, hashLength);
    }

    // https://tools.ietf.org/html/rfc8446#section-4.6.1
    // "The PSK associated with the ticket is computed as:
    //       HKDF-Expand-Label(resumption_master_secret, "resumption", ticket_nonce, Hash.length)"
    byte[] computePSK(byte[] ticketNonce) throws BadRecordMacAlert {
        return hkdfExpandLabel(resumptionMasterSecret, "resumption", ticketNonce, hashLength);
    }

    public byte[] hkdfExpandLabel(byte[] secret, String label, String context, short length) throws BadRecordMacAlert {
        return hkdfExpandLabel(secret, label, context.getBytes(ISO_8859_1), length);
    }

    private byte[] hkdfExpandLabel(byte[] secret, String label, byte[] context, short length) throws BadRecordMacAlert {
        // See https://tools.ietf.org/html/rfc8446#section-7.1 for definition of HKDF-Expand-Label.
        ByteBuffer hkdfLabel = ByteBuffer.allocate(2 + 1 + labelPrefix.length() + label.getBytes(ISO_8859_1).length + 1 + context.length);
        hkdfLabel.putShort(length);
        hkdfLabel.put((byte) (labelPrefix.length() + label.getBytes().length));
        hkdfLabel.put(labelPrefix.getBytes(ISO_8859_1));
        hkdfLabel.put(label.getBytes(ISO_8859_1));
        hkdfLabel.put((byte) (context.length));
        hkdfLabel.put(context);
        return expandHmac(mac, secret, hkdfLabel.array(), length);
    }

    public byte[] getClientHandshakeTrafficSecret() {
        return clientHandshakeTrafficSecret;
    }

    public byte[] getServerHandshakeTrafficSecret() {
        return serverHandshakeTrafficSecret;
    }

    public byte[] getClientApplicationTrafficSecret() {
        return clientApplicationTrafficSecret;
    }

    public byte[] getServerApplicationTrafficSecret() {
        return serverApplicationTrafficSecret;
    }

    public void setOwnKey(PrivateKey clientPrivateKey) {
        this.clientPrivateKey = clientPrivateKey;
    }

    public void setPskSelected() {
        pskSelected = true;
    }

    public void setNoPskSelected() throws BadRecordMacAlert {
        if (psk != null && !pskSelected) {
            // Recompute early secret, as psk is not accepted by server.
            // https://tools.ietf.org/html/rfc8446#section-7.1
            // "... if no PSK is selected, it will then need to compute the Early Secret corresponding to the zero PSK."
            computeEarlySecret(new byte[hashLength]);
        }
    }

    public void setPeerKey(PublicKey serverSharedKey) {
        this.serverSharedKey = serverSharedKey;
    }
}
