package threads.lite.cid;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import merkledag.pb.Merkledag;

@Entity
public record Block(
        @PrimaryKey @NonNull @ColumnInfo(name = "cid") @TypeConverters(Cid.class) Cid cid,
        @NonNull @ColumnInfo(name = "data", typeAffinity = ColumnInfo.BLOB) byte[] data) {


    @NonNull
    public static Block createBlock(@NonNull Cid cid, @NonNull Merkledag.PBNode data) {
        return new Block(cid, data.toByteArray());
    }

    @NonNull
    public static Block createBlock(@NonNull Merkledag.PBNode data) throws Exception {
        byte[] bytes = data.toByteArray();
        Cid cid = Cid.createCidV1(Multicodec.DagProtobuf, bytes);
        return new Block(cid, bytes);
    }

    @NonNull
    public Merkledag.PBNode node() {
        try {
            return Merkledag.PBNode.parseFrom(data);
        } catch (Throwable throwable) {
            throw new RuntimeException(throwable);
        }
    }

    @Override
    @NonNull
    public Cid cid() {
        return cid;
    }

    @Override
    @NonNull
    public byte[] data() {
        return data;
    }


}
