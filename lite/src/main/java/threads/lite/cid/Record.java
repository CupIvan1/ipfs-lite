package threads.lite.cid;

import java.nio.ByteBuffer;

import threads.lite.core.DataHandler;

public record Record(String domain, byte[] type) {
    // PeerRecordEnvelopePayloadType is the type hint used to identify peer records in an Envelope.
    // Defined in https://github.com/multiformats/multicodec/blob/master/table.csv
    // with name "libp2p-peer-record".
    public static final Record PEER = new Record("libp2p-peer-record", new byte[]{0x03, 0x01});

    public static final Record LITE = new Record("lite-record", new byte[]{0x04, 0x04});


    public static byte[] unsignedEnvelopePayload(Record record, byte[] payload) {

        byte[] domain = record.domain().getBytes();
        byte[] payloadType = record.type();

        int domainLength = DataHandler.unsignedVariantSize(domain.length);
        int payloadTypeLength = DataHandler.unsignedVariantSize(payloadType.length);
        int payloadLength = DataHandler.unsignedVariantSize(payload.length);

        ByteBuffer buffer = ByteBuffer.allocate(domain.length + domainLength +
                payloadType.length + payloadTypeLength + payloadLength + payload.length);

        DataHandler.writeUnsignedVariant(buffer, domain.length);
        buffer.put(domain);
        DataHandler.writeUnsignedVariant(buffer, payloadType.length);
        buffer.put(payloadType);
        DataHandler.writeUnsignedVariant(buffer, payload.length);
        buffer.put(payload);
        return buffer.array();
    }

}
