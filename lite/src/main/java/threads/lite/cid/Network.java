package threads.lite.cid;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkCapabilities;

import androidx.annotation.NonNull;

import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import threads.lite.LogUtils;

public interface Network {
    String TAG = Network.class.getSimpleName();

    static boolean isNetworkConnected(@NonNull Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager)
                context.getSystemService(Context.CONNECTIVITY_SERVICE);
        android.net.Network nw = connectivityManager.getActiveNetwork();
        if (nw == null) return false;
        NetworkCapabilities actNw = connectivityManager.getNetworkCapabilities(nw);
        return actNw != null && (actNw.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)
                || actNw.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)
                || actNw.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET));
    }

    @NonNull
    static List<InetSocketAddress> getSiteLocalAddress(int port) throws Exception {
        return getSiteLocalAddresses(port);
    }

    @NonNull
    static List<InetSocketAddress> getSiteLocalAddresses(int port) throws SocketException {
        List<InetSocketAddress> multiaddrs = new ArrayList<>();
        List<NetworkInterface> interfaces = Collections.list(
                NetworkInterface.getNetworkInterfaces());
        for (NetworkInterface networkInterface : interfaces) {
            if (networkInterface.isUp()) {
                List<InetAddress> addresses =
                        Collections.list(networkInterface.getInetAddresses());
                for (InetAddress inetAddress : addresses) {
                    if (inetAddress.isSiteLocalAddress()) {
                        try {
                            multiaddrs.add(new InetSocketAddress(inetAddress, port));
                        } catch (Throwable throwable) {
                            LogUtils.error(TAG, throwable);
                        }
                    }
                }
            }
        }
        return multiaddrs;
    }

    static Collection<InetAddress> networkAddresses() {
        List<InetAddress> inetAddresses = new ArrayList<>();

        try {
            List<NetworkInterface> interfaces = Collections.list(
                    NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface networkInterface : interfaces) {
                if (networkInterface.isUp()) {
                    List<InetAddress> addresses =
                            Collections.list(networkInterface.getInetAddresses());
                    for (InetAddress inetAddress : addresses) {
                        if (!isLocalAddress(inetAddress)) {
                            inetAddresses.add(inetAddress);
                        }
                    }
                }
            }

        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable); // should not occur
        }
        return inetAddresses;
    }

    static boolean isWellKnownIPv4Translatable(InetAddress inetAddress) {
        if (inetAddress instanceof Inet6Address inet6Address) {
            return Network.isWellKnownIPv4Translatable(inet6Address);
        }
        return false;
    }

    /**
     * Whether the address has the well-known prefix for IPv4 translatable addresses
     * as in rfc 6052 and 6144
     */
    private static boolean isWellKnownIPv4Translatable(Inet6Address inet6Address) {
        byte[] segments = inet6Address.getAddress();
        // 64:ff9b::/96 prefix for auto ipv4/ipv6 translation
        // Segments are [0, 100, -1, -101, 0, 0, 0, 0, 0, 0, 0, 0, x, x, x, x]
        return segments[0] == 0 && segments[1] == 100 && segments[2] == -1 && segments[3] == -101 &&
                segments[4] == 0 && segments[5] == 0 && segments[6] == 0 && segments[7] == 0 &&
                segments[8] == 0 && segments[9] == 0 && segments[10] == 0 && segments[11] == 0;
    }

    @SuppressWarnings("BooleanMethodIsAlwaysInverted")
    static boolean isLocalAddress(InetAddress inetAddress) {
        return inetAddress.isAnyLocalAddress() || inetAddress.isLinkLocalAddress()
                || (inetAddress.isLoopbackAddress())
                || (inetAddress.isSiteLocalAddress());
    }

}
