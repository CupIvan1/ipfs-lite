/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package threads.lite.cid;


import java.nio.charset.StandardCharsets;

/**
 * Provides Base32 encoding and decoding as defined by <a href="http://www.ietf.org/rfc/rfc4648.txt">RFC 4648</a>.
 * <p>
 * From https://commons.apache.org/proper/commons-codec/
 *
 * <p>
 * The class can be parameterized in the following manner with various constructors:
 * </p>
 * <ul>
 * <li>Whether to use the "base32hex" variant instead of the default "base32"</li>
 * <li>Line length: Default 76. Line length that aren't multiples of 8 will still essentially end up being multiples of
 * 8 in the encoded data.
 * <li>Line separator: Default is CRLF ("\r\n")</li>
 * </ul>
 * <p>
 * This class operates directly on byte streams, and not character streams.
 * </p>
 * <p>
 * This class is thread-safe.
 * </p>
 *
 * @version $Id$
 * @see <a href="http://www.ietf.org/rfc/rfc4648.txt">RFC 4648</a>
 * @since 1.5
 */
interface Base32 {

    /**
     * Mask used to extract 8 bits, used in decoding bytes
     */
    int MASK_8BITS = 0xff;
    /**
     * Byte used to pad output.
     */
    byte PAD_DEFAULT = '='; // Allow static access to default
    int BYTES_PER_ENCODED_BLOCK = 8;
    /**
     * EOF
     *
     * @since 1.7
     */
    int EOF = -1;
    /**
     * BASE32 characters are 5 bits in length.
     * They are formed by taking a block of five octets to form a 40-bit string,
     * which is converted into eight BASE32 characters.
     */
    int BITS_PER_ENCODED_BYTE = 5;
    int BYTES_PER_UNENCODED_BLOCK = 5;
    /**
     * This array is a lookup table that translates Unicode characters drawn from the "Base32 Alphabet" (as specified
     * in Table 3 of RFC 4648) into their 5-bit positive integer equivalents. Characters that are not in the Base32
     * alphabet but fall within the bounds of the array are translated to -1.
     */
    byte[] DECODE_TABLE = {
            //  0   1   2   3   4   5   6   7   8   9   A   B   C   D   E   F
            -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, // 00-0f
            -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, // 10-1f
            -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, // 20-2f
            -1, -1, 26, 27, 28, 29, 30, 31, -1, -1, -1, -1, -1, -1, -1, -1, // 30-3f 2-7
            -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, // 40-4f A-O
            15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25,                     // 50-5a P-Z
            -1, -1, -1, -1, -1, // 5b - 5f
            -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, // 60 - 6f a-o
            15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25,                     // 70 - 7a p-z/**/
    };

    // The static final fields above are used for the original static byte[] methods on Base32.
    // The private member fields below are used with the new streaming approach, which requires
    // some state be preserved between calls of encode() and decode().
    /**
     * This array is a lookup table that translates 5-bit positive integer index values into their "Base32 Alphabet"
     * equivalents as specified in Table 3 of RFC 4648.
     */
    byte[] ENCODE_TABLE = {
            'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M',
            'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
            '2', '3', '4', '5', '6', '7',
    };

    /**
     * Mask used to extract 5 bits, used when encoding Base32 bytes
     */
    int MASK_5BITS = 0x1f;
    int DEFAULT_BUFFER_RESIZE_FACTOR = 2;
    /**
     * Defines the default buffer size - currently {@value}
     * - must be large enough for at least one encoded block+separator
     */
    int DEFAULT_BUFFER_SIZE = 8192;


    /**
     * <p>
     * Decodes all of the provided data, starting at inPos, for inAvail bytes. Should be called at least twice: once
     * with the data to decode, and once with inAvail set to "-1" to alert decoder that EOF has been reached. The "-1"
     * call is not necessary when decoding, but it doesn't hurt, either.
     * </p>
     * <p>
     * Ignores all non-Base32 characters. This is how chunked (e.g. 76 character) data is handled, since CR and LF are
     * silently ignored, but has implications for other bytes, too. This method subscribes to the garbage-in,
     * garbage-out philosophy: it will not check the provided data for validity.
     * </p>
     *
     * @param in      byte[] array of ascii data to Base32 decode.
     * @param inAvail Amount of bytes available from input for encoding.
     * @param context the context to be used
     *                <p>
     *                Output is written to {@link Context#buffer} as 8-bit octets, using {@link Context#pos} as the buffer position
     */
    static void decode(final byte[] in, final int inAvail, final Context context) {
        // package protected for access from I/O streams
        int inPos = 0;
        if (context.eof) {
            return;
        }
        if (inAvail < 0) {
            context.eof = true;
        }
        for (int i = 0; i < inAvail; i++) {
            final byte b = in[inPos++];
            if (b == PAD_DEFAULT) {
                // We're done.
                context.eof = true;
                break;
            }
            final byte[] buffer = ensureBufferSize(BYTES_PER_ENCODED_BLOCK - 1, context);
            if (b >= 0 && b < DECODE_TABLE.length) {
                final int result = DECODE_TABLE[b];
                if (result >= 0) {
                    context.modulus = (context.modulus + 1) % BYTES_PER_ENCODED_BLOCK;
                    // collect decoded bytes
                    context.lbitWorkArea = (context.lbitWorkArea << BITS_PER_ENCODED_BYTE) + result;
                    if (context.modulus == 0) { // we can output the 5 bytes
                        buffer[context.pos++] = (byte) ((context.lbitWorkArea >> 32) & MASK_8BITS);
                        buffer[context.pos++] = (byte) ((context.lbitWorkArea >> 24) & MASK_8BITS);
                        buffer[context.pos++] = (byte) ((context.lbitWorkArea >> 16) & MASK_8BITS);
                        buffer[context.pos++] = (byte) ((context.lbitWorkArea >> 8) & MASK_8BITS);
                        buffer[context.pos++] = (byte) (context.lbitWorkArea & MASK_8BITS);
                    }
                }
            }
        }

        // Two forms of EOF as far as Base32 decoder is concerned: actual
        // EOF (-1) and first time '=' character is encountered in stream.
        // This approach makes the '=' padding characters completely optional.
        if (context.eof && context.modulus >= 2) { // if modulus < 2, nothing to do
            final byte[] buffer = ensureBufferSize(BYTES_PER_ENCODED_BLOCK - 1, context);

            //  we ignore partial bytes, i.e. only multiples of 8 count
            switch (context.modulus) {
                case 2 -> // 10 bits, drop 2 and output one byte
                        buffer[context.pos++] = (byte) ((context.lbitWorkArea >> 2) & MASK_8BITS);
                case 3 -> // 15 bits, drop 7 and output 1 byte
                        buffer[context.pos++] = (byte) ((context.lbitWorkArea >> 7) & MASK_8BITS);
                case 4 -> { // 20 bits = 2*8 + 4
                    context.lbitWorkArea = context.lbitWorkArea >> 4; // drop 4 bits
                    buffer[context.pos++] = (byte) ((context.lbitWorkArea >> 8) & MASK_8BITS);
                    buffer[context.pos++] = (byte) ((context.lbitWorkArea) & MASK_8BITS);
                }
                case 5 -> { // 25bits = 3*8 + 1
                    context.lbitWorkArea = context.lbitWorkArea >> 1;
                    buffer[context.pos++] = (byte) ((context.lbitWorkArea >> 16) & MASK_8BITS);
                    buffer[context.pos++] = (byte) ((context.lbitWorkArea >> 8) & MASK_8BITS);
                    buffer[context.pos++] = (byte) ((context.lbitWorkArea) & MASK_8BITS);
                }
                case 6 -> { // 30bits = 3*8 + 6
                    context.lbitWorkArea = context.lbitWorkArea >> 6;
                    buffer[context.pos++] = (byte) ((context.lbitWorkArea >> 16) & MASK_8BITS);
                    buffer[context.pos++] = (byte) ((context.lbitWorkArea >> 8) & MASK_8BITS);
                    buffer[context.pos++] = (byte) ((context.lbitWorkArea) & MASK_8BITS);
                }
                case 7 -> { // 35 = 4*8 +3
                    context.lbitWorkArea = context.lbitWorkArea >> 3;
                    buffer[context.pos++] = (byte) ((context.lbitWorkArea >> 24) & MASK_8BITS);
                    buffer[context.pos++] = (byte) ((context.lbitWorkArea >> 16) & MASK_8BITS);
                    buffer[context.pos++] = (byte) ((context.lbitWorkArea >> 8) & MASK_8BITS);
                    buffer[context.pos++] = (byte) ((context.lbitWorkArea) & MASK_8BITS);
                }
                default ->
                    // modulus can be 0-7, and we excluded 0,1 already
                        throw new IllegalStateException("Impossible modulus " + context.modulus);
            }
        }
    }

    /**
     * <p>
     * Encodes all of the provided data, starting at inPos, for inAvail bytes. Must be called at least twice: once with
     * the data to encode, and once with inAvail set to "-1" to alert encoder that EOF has been reached, so flush last
     * remaining bytes (if not multiple of 5).
     * </p>
     *
     * @param in      byte[] array of binary data to Base32 encode.
     * @param inPos   Position to start reading data from.
     * @param inAvail Amount of bytes available from input for encoding.
     * @param context the context to be used
     */

    static void encode(final byte[] in, int inPos, final int inAvail, final Context context) {
        // package protected for access from I/O streams

        if (context.eof) {
            return;
        }
        // inAvail < 0 is how we're informed of EOF in the underlying data we're
        // encoding.
        if (inAvail < 0) {
            context.eof = true;
            if (0 == context.modulus) {
                return; // no leftovers to process and not using chunking
            }
            final byte[] buffer = ensureBufferSize(BYTES_PER_ENCODED_BLOCK, context);
            switch (context.modulus) { // % 5
                case 0:
                    break;
                case 1: // Only 1 octet; take top 5 bits then remainder
                    buffer[context.pos++] = ENCODE_TABLE[(int) (context.lbitWorkArea >> 3) & MASK_5BITS]; // 8-1*5 = 3
                    buffer[context.pos++] = ENCODE_TABLE[(int) (context.lbitWorkArea << 2) & MASK_5BITS]; // 5-3=2
                    buffer[context.pos++] = PAD_DEFAULT;
                    buffer[context.pos++] = PAD_DEFAULT;
                    buffer[context.pos++] = PAD_DEFAULT;
                    buffer[context.pos++] = PAD_DEFAULT;
                    buffer[context.pos++] = PAD_DEFAULT;
                    buffer[context.pos++] = PAD_DEFAULT;
                    break;
                case 2: // 2 octets = 16 bits to use
                    buffer[context.pos++] = ENCODE_TABLE[(int) (context.lbitWorkArea >> 11) & MASK_5BITS]; // 16-1*5 = 11
                    buffer[context.pos++] = ENCODE_TABLE[(int) (context.lbitWorkArea >> 6) & MASK_5BITS]; // 16-2*5 = 6
                    buffer[context.pos++] = ENCODE_TABLE[(int) (context.lbitWorkArea >> 1) & MASK_5BITS]; // 16-3*5 = 1
                    buffer[context.pos++] = ENCODE_TABLE[(int) (context.lbitWorkArea << 4) & MASK_5BITS]; // 5-1 = 4
                    buffer[context.pos++] = PAD_DEFAULT;
                    buffer[context.pos++] = PAD_DEFAULT;
                    buffer[context.pos++] = PAD_DEFAULT;
                    buffer[context.pos++] = PAD_DEFAULT;
                    break;
                case 3: // 3 octets = 24 bits to use
                    buffer[context.pos++] = ENCODE_TABLE[(int) (context.lbitWorkArea >> 19) & MASK_5BITS]; // 24-1*5 = 19
                    buffer[context.pos++] = ENCODE_TABLE[(int) (context.lbitWorkArea >> 14) & MASK_5BITS]; // 24-2*5 = 14
                    buffer[context.pos++] = ENCODE_TABLE[(int) (context.lbitWorkArea >> 9) & MASK_5BITS]; // 24-3*5 = 9
                    buffer[context.pos++] = ENCODE_TABLE[(int) (context.lbitWorkArea >> 4) & MASK_5BITS]; // 24-4*5 = 4
                    buffer[context.pos++] = ENCODE_TABLE[(int) (context.lbitWorkArea << 1) & MASK_5BITS]; // 5-4 = 1
                    buffer[context.pos++] = PAD_DEFAULT;
                    buffer[context.pos++] = PAD_DEFAULT;
                    buffer[context.pos++] = PAD_DEFAULT;
                    break;
                case 4: // 4 octets = 32 bits to use
                    buffer[context.pos++] = ENCODE_TABLE[(int) (context.lbitWorkArea >> 27) & MASK_5BITS]; // 32-1*5 = 27
                    buffer[context.pos++] = ENCODE_TABLE[(int) (context.lbitWorkArea >> 22) & MASK_5BITS]; // 32-2*5 = 22
                    buffer[context.pos++] = ENCODE_TABLE[(int) (context.lbitWorkArea >> 17) & MASK_5BITS]; // 32-3*5 = 17
                    buffer[context.pos++] = ENCODE_TABLE[(int) (context.lbitWorkArea >> 12) & MASK_5BITS]; // 32-4*5 = 12
                    buffer[context.pos++] = ENCODE_TABLE[(int) (context.lbitWorkArea >> 7) & MASK_5BITS]; // 32-5*5 =  7
                    buffer[context.pos++] = ENCODE_TABLE[(int) (context.lbitWorkArea >> 2) & MASK_5BITS]; // 32-6*5 =  2
                    buffer[context.pos++] = ENCODE_TABLE[(int) (context.lbitWorkArea << 3) & MASK_5BITS]; // 5-2 = 3
                    buffer[context.pos++] = PAD_DEFAULT;
                    break;
                default:
                    throw new IllegalStateException("Impossible modulus " + context.modulus);
            }

        } else {
            for (int i = 0; i < inAvail; i++) {
                final byte[] buffer = ensureBufferSize(BYTES_PER_ENCODED_BLOCK, context);
                context.modulus = (context.modulus + 1) % BYTES_PER_UNENCODED_BLOCK;
                int b = in[inPos++];
                if (b < 0) {
                    b += 256;
                }
                context.lbitWorkArea = (context.lbitWorkArea << 8) + b; // BITS_PER_BYTE
                if (0 == context.modulus) { // we have enough bytes to create our output
                    buffer[context.pos++] = ENCODE_TABLE[(int) (context.lbitWorkArea >> 35) & MASK_5BITS];
                    buffer[context.pos++] = ENCODE_TABLE[(int) (context.lbitWorkArea >> 30) & MASK_5BITS];
                    buffer[context.pos++] = ENCODE_TABLE[(int) (context.lbitWorkArea >> 25) & MASK_5BITS];
                    buffer[context.pos++] = ENCODE_TABLE[(int) (context.lbitWorkArea >> 20) & MASK_5BITS];
                    buffer[context.pos++] = ENCODE_TABLE[(int) (context.lbitWorkArea >> 15) & MASK_5BITS];
                    buffer[context.pos++] = ENCODE_TABLE[(int) (context.lbitWorkArea >> 10) & MASK_5BITS];
                    buffer[context.pos++] = ENCODE_TABLE[(int) (context.lbitWorkArea >> 5) & MASK_5BITS];
                    buffer[context.pos++] = ENCODE_TABLE[(int) context.lbitWorkArea & MASK_5BITS];
                }
            }
        }
    }

    /**
     * Returns the amount of buffered data available for reading.
     *
     * @param context the context to be used
     * @return The amount of buffered data available for reading.
     */
    static int available(final Context context) {  // package protected for access from I/O streams
        return context.buffer != null ? context.pos - context.readPos : 0;
    }


    /**
     * Increases our buffer by the {@link #DEFAULT_BUFFER_RESIZE_FACTOR}.
     *
     * @param context the context to be used
     */
    private static byte[] resizeBuffer(final Context context) {
        if (context.buffer == null) {
            context.buffer = new byte[DEFAULT_BUFFER_SIZE];
            context.pos = 0;
            context.readPos = 0;
        } else {
            final byte[] b = new byte[context.buffer.length * DEFAULT_BUFFER_RESIZE_FACTOR];
            System.arraycopy(context.buffer, 0, b, 0, context.buffer.length);
            context.buffer = b;
        }
        return context.buffer;
    }

    /**
     * Ensure that the buffer has room for <code>size</code> bytes
     *
     * @param size    minimum spare space required
     * @param context the context to be used
     * @return the buffer
     */
    static byte[] ensureBufferSize(final int size, final Context context) {
        if ((context.buffer == null) || (context.buffer.length < context.pos + size)) {
            return resizeBuffer(context);
        }
        return context.buffer;
    }

    /**
     * Extracts buffered data into the provided byte[] array, starting at position bPos, up to a maximum of bAvail
     * bytes. Returns how many bytes were actually extracted.
     * <p>
     * Package protected for access from I/O streams.
     *
     * @param b       byte[] array to extract the buffered data into.
     * @param bAvail  amount of bytes we're allowed to extract. We may extract fewer (if fewer are available).
     * @param context the context to be used
     */
    private static void readResults(final byte[] b, final int bAvail, final Context context) {
        if (context.buffer != null) {
            final int len = Math.min(available(context), bAvail);
            System.arraycopy(context.buffer, context.readPos, b, 0, len);
            context.readPos += len;
            if (context.readPos >= context.pos) {
                context.buffer = null; // so hasData() will return false, and this method can return -1
            }
        }
    }


    /**
     * Decodes a String containing characters in the Base-N alphabet.
     *
     * @param pArray A String containing Base-N character data
     * @return a byte array containing binary data
     */
    static byte[] decode(final String pArray) {
        return decode(pArray.getBytes(StandardCharsets.UTF_8));
    }

    /**
     * Decodes a byte[] containing characters in the Base-N alphabet.
     *
     * @param pArray A byte array containing Base-N character data
     * @return a byte array containing binary data
     */
    static byte[] decode(final byte[] pArray) {
        if (pArray == null || pArray.length == 0) {
            return pArray;
        }
        final Context context = new Context();
        decode(pArray, pArray.length, context);
        decode(pArray, EOF, context); // Notify decoder of EOF.
        final byte[] result = new byte[context.pos];
        readResults(result, result.length, context);
        return result;
    }

    /**
     * Encodes a byte[] containing binary data, into a byte[] containing characters in the alphabet.
     *
     * @param pArray a byte array containing binary data
     * @return A byte array containing only the base N alphabetic character data
     */
    static byte[] encode(final byte[] pArray) {
        if (pArray == null || pArray.length == 0) {
            return pArray;
        }
        return encode(pArray, 0, pArray.length);
    }

    /**
     * Encodes a byte[] containing binary data, into a byte[] containing
     * characters in the alphabet.
     *
     * @param pArray a byte array containing binary data
     * @param offset initial offset of the subarray.
     * @param length length of the subarray.
     * @return A byte array containing only the base N alphabetic character data
     * @since 1.11
     */
    static byte[] encode(final byte[] pArray, final int offset, final int length) {
        if (pArray == null || pArray.length == 0) {
            return pArray;
        }
        final Context context = new Context();
        encode(pArray, offset, length, context);
        encode(pArray, offset, EOF, context); // Notify encoder of EOF.
        final byte[] buf = new byte[context.pos - context.readPos];
        readResults(buf, buf.length, context);
        return buf;
    }


    /**
     * Holds thread context so classes can be thread-safe.
     * <p>
     * This class is not itself thread-safe; each thread must allocate its own copy.
     *
     * @since 1.7
     */
    class Context {

        /**
         * Place holder for the bytes we're dealing with for our based logic.
         * Bitwise operations store and extract the encoding or decoding from this variable.
         */
        long lbitWorkArea;

        /**
         * Buffer for streaming.
         */
        byte[] buffer;

        /**
         * Position where next character should be written in the buffer.
         */
        int pos;

        /**
         * Position where next character should be read from the buffer.
         */
        int readPos;

        /**
         * Boolean flag to indicate the EOF has been reached. Once EOF has been reached, this object becomes useless,
         * and must be thrown away.
         */
        boolean eof;

        /**
         * Writes to the buffer only occur after every 3/5 reads when encoding, and every 4/8 reads when decoding. This
         * variable helps track that.
         */
        int modulus;

        Context() {
        }
    }
}
