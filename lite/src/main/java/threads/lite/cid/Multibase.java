package threads.lite.cid;


public interface Multibase {
    char BASE32 = 'b';
    char BASE36 = 'k';
    char BASE58 = 'z';

    static String encode(char b, byte[] data) {
        return switch (b) {
            case BASE58 -> BASE58 + Base58.encode(data);
            case BASE32 ->
                    BASE32 + new String(Base32.encode(data)).toLowerCase().replaceAll("=", "");
            case BASE36 -> BASE36 + Base36.encode(data);
            default -> throw new IllegalStateException("Unexpected value: " + b);
        };
    }

    static byte[] decode(String data) throws Exception {
        char b = data.charAt(0);
        String rest = data.substring(1);
        return switch (b) {
            case BASE58 -> Base58.decode(rest);
            case BASE32 -> Base32.decode(rest);
            case BASE36 -> Base36.decode(rest);
            default -> throw new IllegalStateException("Unexpected value: " + b);
        };
    }
}
