package threads.lite.core;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public abstract class TimeoutProgress implements Progress {

    private final long timeout;
    @Nullable
    private final Cancellable cancellable;
    private final long start;

    protected TimeoutProgress(long timeout) {
        this.cancellable = null;
        this.timeout = timeout;
        this.start = System.currentTimeMillis();
    }

    protected TimeoutProgress(@NonNull Cancellable cancellable, long timeout) {
        this.cancellable = cancellable;
        this.timeout = timeout;
        this.start = System.currentTimeMillis();
    }

    @Override
    public long timeout() {
        return timeout;
    }

    @Override
    public boolean isCancelled() {
        if (cancellable != null) {
            return cancellable.isCancelled() || (System.currentTimeMillis() - start) > (timeout * 1000);
        }
        return (System.currentTimeMillis() - start) > (timeout * 1000);
    }


}
