package threads.lite.core;

import androidx.annotation.NonNull;

import java.util.function.Consumer;

import record.pb.RecordOuterClass;
import threads.lite.cid.Cid;
import threads.lite.cid.Multiaddr;
import threads.lite.cid.PeerId;

public interface Routing {

    void putValue(@NonNull Cancellable cancellable, @NonNull Consumer<Multiaddr> consumer,
                  @NonNull RecordOuterClass.Record record);

    void findProviders(@NonNull Cancellable cancellable, @NonNull Consumer<Multiaddr> consumer,
                       @NonNull Cid cid);

    void findPeer(@NonNull Cancellable cancellable, @NonNull Consumer<Multiaddr> consumer,
                  @NonNull PeerId peerId);

    void searchValue(@NonNull Cancellable cancellable,
                     @NonNull Consumer<Page> consumer, @NonNull byte[] target);

    void findClosestPeers(@NonNull Cancellable cancellable,
                          @NonNull Consumer<Multiaddr> consumer, @NonNull PeerId peerId);

}
