package threads.lite.core;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import threads.lite.cid.Block;
import threads.lite.cid.Cid;

public interface BlockStore {

    boolean hasBlock(@NonNull Cid cid);

    @Nullable
    Block getBlock(@NonNull Cid cid);

    @Nullable
    byte[] getBlockData(@NonNull Cid cid);

    void deleteBlock(@NonNull Cid cid);

    void storeBlock(@NonNull Block block);

    void clear();
}


