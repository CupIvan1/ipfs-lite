package threads.lite.core;


import threads.lite.quic.Stream;

// Note: a protocol handler is only invoked, when a remote peer initiate a
// stream over an existing connection
public interface ProtocolHandler {

    // Note this function is only invoked when ALPN is "libp2p"
    // is invoked, when the your protocol is requested
    void protocol(Stream stream) throws Exception;

    // when alpn is "lite" the fin is included (this function is only invoked, when fin is received)
    // is invoked, when data is arriving associated with your protocol
    void data(String alpn, Stream stream, byte[] data) throws Exception;

    // Note this function is only invoked when ALPN is "libp2p"
    void fin(Stream stream);

}
