package threads.lite.relay;

import java.util.Objects;

import circuit.pb.Circuit;
import threads.lite.IPFS;
import threads.lite.LogUtils;
import threads.lite.cid.PeerId;
import threads.lite.core.DataHandler;
import threads.lite.core.ProtocolHandler;
import threads.lite.quic.Stream;
import threads.lite.quic.StreamRequester;

public final class RelayStopHandler implements ProtocolHandler {

    @Override
    public void protocol(Stream stream) {
        stream.writeOutput(DataHandler.encodeProtocols(IPFS.RELAY_PROTOCOL_STOP), false);
    }

    @Override
    public void data(String alpn, Stream stream, byte[] data) throws Exception {

        Circuit.StopMessage stopMessage = Circuit.StopMessage.parseFrom(data);
        Objects.requireNonNull(stopMessage);

        if (stopMessage.hasPeer()) {
            PeerId peerId = PeerId.create(stopMessage.getPeer().getId().toByteArray());
            Circuit.StopMessage.Builder builder =
                    Circuit.StopMessage.newBuilder()
                            .setType(Circuit.StopMessage.Type.STATUS);
            builder.setStatus(Circuit.Status.OK);
            stream.setAttribute(StreamRequester.PEER, peerId);
            stream.writeOutput(DataHandler.encode(builder.build()), false);
        } else {
            Circuit.StopMessage.Builder builder =
                    Circuit.StopMessage.newBuilder()
                            .setType(Circuit.StopMessage.Type.STATUS);
            builder.setStatus(Circuit.Status.MALFORMED_MESSAGE);
            stream.writeOutput(DataHandler.encode(builder.build()), false);
        }

    }

    @Override
    public void fin(Stream stream) {
        // this is invoked, that is good, but request is finished when message is parsed [ok]
        LogUtils.error(getClass().getSimpleName(), "fin received " +
                stream.connection().remotePeerId());
    }

}
