package threads.lite.crypto;


import androidx.annotation.NonNull;

import crypto.pb.Crypto;

public interface PubKey extends Key {


    void verify(byte[] data, byte[] signature) throws Exception;


    @NonNull
    Crypto.KeyType getKeyType();


}

