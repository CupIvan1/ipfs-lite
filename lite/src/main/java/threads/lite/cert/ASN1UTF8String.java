package threads.lite.cert;

import androidx.annotation.NonNull;

import java.io.IOException;

public abstract class ASN1UTF8String extends ASN1Primitive implements ASN1String {
    private final byte[] contents;

    ASN1UTF8String(String string) {
        this(Strings.toUTF8ByteArray(string));
    }

    ASN1UTF8String(byte[] contents) {
        this.contents = contents;
    }

    static ASN1UTF8String createPrimitive(byte[] contents) {
        return new DERUTF8String(contents);
    }

    public final String getString() {
        return Strings.fromUTF8ByteArray(contents);
    }

    @NonNull
    public String toString() {
        return getString();
    }


    final boolean asn1Equals(ASN1Primitive other) {
        if (!(other instanceof ASN1UTF8String that)) {
            return false;
        }

        return Arrays.areEqual(this.contents, that.contents);
    }

    final boolean encodeConstructed() {
        return false;
    }

    final int encodedLength(boolean withTag) {
        return ASN1OutputStream.getLengthOfEncodingDL(withTag, contents.length);
    }

    final void encode(ASN1OutputStream out, boolean withTag) throws IOException {
        out.writeEncodingDL(withTag, BERTags.UTF8_STRING, contents);
    }
}
