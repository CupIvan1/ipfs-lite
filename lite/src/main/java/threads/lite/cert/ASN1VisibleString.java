package threads.lite.cert;

import androidx.annotation.NonNull;

import java.io.IOException;

/**
 * ASN.1 VisibleString object encoding ISO 646 (ASCII) character code points 32 to 126.
 * <p>
 * Explicit character set escape sequences are not allowed.
 * </p>
 */
public abstract class ASN1VisibleString extends ASN1Primitive implements ASN1String {
    private final byte[] contents;

    ASN1VisibleString(byte[] contents) {
        this.contents = contents;
    }

    static ASN1VisibleString createPrimitive(byte[] contents) {
        return new DERVisibleString(contents);
    }

    public final String getString() {
        return Strings.fromByteArray(contents);
    }

    @NonNull
    public String toString() {
        return getString();
    }

    final boolean encodeConstructed() {
        return false;
    }

    final int encodedLength(boolean withTag) {
        return ASN1OutputStream.getLengthOfEncodingDL(withTag, contents.length);
    }

    final void encode(ASN1OutputStream out, boolean withTag) throws IOException {
        out.writeEncodingDL(withTag, BERTags.VISIBLE_STRING, contents);
    }

    final boolean asn1Equals(ASN1Primitive other) {
        if (!(other instanceof ASN1VisibleString that)) {
            return false;
        }

        return Arrays.areEqual(this.contents, that.contents);
    }

}
