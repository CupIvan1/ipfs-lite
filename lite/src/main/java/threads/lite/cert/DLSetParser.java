package threads.lite.cert;

import java.io.IOException;

/**
 * Parser class for DL SETs.
 * <p>
 */
public final class DLSetParser implements ASN1Encodable, InMemoryRepresentable {
    private final ASN1StreamParser _parser;

    DLSetParser(ASN1StreamParser parser) {
        this._parser = parser;
    }

    /**
     * Return an in memory, encodable, representation of the SET.
     *
     * @return a DLSet.
     * @throws IOException if there is an issue loading the data.
     */
    public ASN1Primitive getLoadedObject() throws IOException {
        return DLFactory.createSet(_parser.readVector());
    }

    /**
     * Return a DLSet representing this parser and its contents.
     *
     * @return a DLSet
     */
    public ASN1Primitive toASN1Primitive() {
        try {
            return getLoadedObject();
        } catch (IOException e) {
            throw new ASN1ParsingException(e.getMessage(), e);
        }
    }
}
