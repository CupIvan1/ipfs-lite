package threads.lite.cert;

import java.io.IOException;

/**
 * Parser for indefinite-length SETs.
 */
public final class BERSetParser implements ASN1Encodable, InMemoryRepresentable {
    private final ASN1StreamParser _parser;

    BERSetParser(ASN1StreamParser parser) {
        this._parser = parser;
    }

    static BERSet parse(ASN1StreamParser sp) throws IOException {
        return new BERSet(sp.readVector());
    }

    /**
     * Return an in-memory, encodable, representation of the SET.
     *
     * @return a BERSet.
     * @throws IOException if there is an issue loading the data.
     */
    public ASN1Primitive getLoadedObject()
            throws IOException {
        return parse(_parser);
    }

    /**
     * Return an BERSet representing this parser and its contents.
     *
     * @return an BERSet
     */
    public ASN1Primitive toASN1Primitive() {
        try {
            return getLoadedObject();
        } catch (IOException e) {
            throw new ASN1ParsingException(e.getMessage(), e);
        }
    }
}
