package threads.lite.cert;

import java.io.IOException;
import java.io.InputStream;

/**
 * A parser for indefinite-length BIT STRINGs.
 */
public final class BERBitStringParser implements ASN1BitStringParser {
    private final ASN1StreamParser _parser;

    private ConstructedBitStream _bitStream;

    BERBitStringParser(
            ASN1StreamParser parser) {
        _parser = parser;
    }

    static BERBitString parse(ASN1StreamParser sp) throws IOException {
        ConstructedBitStream bitStream = new ConstructedBitStream(sp);
        byte[] data = Streams.readAll(bitStream);
        int padBits = bitStream.getPadBits();
        return new BERBitString(data, padBits);
    }

    public InputStream getBitStream() {
        return _bitStream = new ConstructedBitStream(_parser);
    }

    public int getPadBits() {
        return _bitStream.getPadBits();
    }

    public ASN1Primitive getLoadedObject()
            throws IOException {
        return parse(_parser);
    }

    public ASN1Primitive toASN1Primitive() {
        try {
            return getLoadedObject();
        } catch (IOException e) {
            throw new ASN1ParsingException("IOException converting stream to byte array: " + e.getMessage(), e);
        }
    }
}
