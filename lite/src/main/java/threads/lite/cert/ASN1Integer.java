package threads.lite.cert;

import androidx.annotation.NonNull;

import java.io.IOException;
import java.math.BigInteger;

/**
 * Class representing the ASN.1 INTEGER type.
 */
public final class ASN1Integer extends ASN1Primitive {
    /**
     * @noinspection AnonymousInnerClass
     */
    private static final ASN1UniversalType TYPE = new ASN1UniversalType(ASN1Integer.class) {
    };

    private static final int SIGN_EXT_SIGNED = 0xFFFFFFFF;
    private static final int SIGN_EXT_UNSIGNED = 0xFF;

    private final byte[] bytes;
    private final int start;

    /**
     * Construct an INTEGER from the passed in long value.
     *
     * @param value the long representing the value desired.
     */
    public ASN1Integer(long value) {
        this.bytes = BigInteger.valueOf(value).toByteArray();
        this.start = 0;
    }

    /**
     * Construct an INTEGER from the passed in BigInteger value.
     *
     * @param value the BigInteger representing the value desired.
     */
    public ASN1Integer(BigInteger value) {
        this.bytes = value.toByteArray();
        this.start = 0;
    }

    private ASN1Integer(byte[] bytes) {
        if (isMalformed(bytes)) {
            throw new IllegalArgumentException("malformed integer");
        }

        this.bytes = bytes;
        this.start = signBytesToSkip(bytes);
    }

    /**
     * Return an integer from the passed in object.
     *
     * @param obj an ASN1Integer or an object that can be converted into one.
     * @return an ASN1Integer instance.
     * @throws IllegalArgumentException if the object cannot be converted.
     */
    public static ASN1Integer getInstance(
            Object obj) {
        if (obj == null || obj instanceof ASN1Integer) {
            return (ASN1Integer) obj;
        }

        if (obj instanceof byte[]) {
            try {
                return (ASN1Integer) TYPE.fromByteArray((byte[]) obj);
            } catch (Exception e) {
                throw new IllegalArgumentException("encoding error in getInstance: " + e);
            }
        }

        throw new IllegalArgumentException("illegal object in getInstance: " + obj.getClass().getName());
    }

    /**
     * Return an Integer from a tagged object.
     *
     * @param taggedObject the tagged object holding the object we want
     * @param explicit     true if the object is meant to be explicitly
     *                     tagged false otherwise.
     * @return an ASN1Integer instance.
     * @throws IllegalArgumentException if the tagged object cannot
     *                                  be converted.
     */
    public static ASN1Integer getInstance(ASN1TaggedObject taggedObject, boolean explicit) {
        return (ASN1Integer) TYPE.getContextInstance(taggedObject, explicit);
    }

    static ASN1Integer createPrimitive(byte[] contents) {
        return new ASN1Integer(contents);
    }

    private static int intValue(byte[] bytes, int start) {
        int length = bytes.length;
        int pos = Math.max(start, length - 4);

        int val = bytes[pos] & ASN1Integer.SIGN_EXT_SIGNED;
        while (++pos < length) {
            val = (val << 8) | (bytes[pos] & SIGN_EXT_UNSIGNED);
        }
        return val;
    }

    /**
     * Apply the correct validation for an INTEGER primitive following the BER rules.
     *
     * @param bytes The raw encoding of the integer.
     * @return true if the (in)put fails this validation.
     */
    static boolean isMalformed(byte[] bytes) {
        return switch (bytes.length) {
            case 0 -> true;
            case 1 -> false;
            default -> bytes[0] == (bytes[1] >> 7);
        };
    }

    private static int signBytesToSkip(byte[] bytes) {
        int pos = 0, last = bytes.length - 1;
        while (pos < last
                && bytes[pos] == (bytes[pos + 1] >> 7)) {
            ++pos;
        }
        return pos;
    }

    private BigInteger getValue() {
        return new BigInteger(bytes);
    }

    public boolean hasValue(int x) {
        return (bytes.length - start) <= 4
                && intValue(bytes, start) == x;
    }

    boolean encodeConstructed() {
        return false;
    }

    int encodedLength(boolean withTag) {
        return ASN1OutputStream.getLengthOfEncodingDL(withTag, bytes.length);
    }

    void encode(ASN1OutputStream out, boolean withTag) throws IOException {
        out.writeEncodingDL(withTag, BERTags.INTEGER, bytes);
    }


    boolean asn1Equals(ASN1Primitive o) {
        if (!(o instanceof ASN1Integer other)) {
            return false;
        }

        return Arrays.areEqual(this.bytes, other.bytes);
    }

    @NonNull
    public String toString() {
        return getValue().toString();
    }
}
