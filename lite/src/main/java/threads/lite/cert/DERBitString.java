package threads.lite.cert;

import java.io.IOException;

/**
 * A BIT STRING with DER encoding - the first byte contains the count of padding bits included in the byte array's last byte.
 */
public final class DERBitString extends ASN1BitString {


    public DERBitString(byte[] data, int padBits) {
        super(data, padBits);
    }

    DERBitString(byte[] contents) {
        super(contents);
    }

    boolean encodeConstructed() {
        return false;
    }

    int encodedLength(boolean withTag) {
        return ASN1OutputStream.getLengthOfEncodingDL(withTag, contents.length);
    }

    void encode(ASN1OutputStream out, boolean withTag) throws IOException {
        int padBits = contents[0] & 0xFF;
        int length = contents.length;
        int last = length - 1;

        byte lastOctet = contents[last];
        byte lastOctetDER = (byte) (contents[last] & (0xFF << padBits));

        if (lastOctet == lastOctetDER) {
            out.writeEncodingDL(withTag, BERTags.BIT_STRING, contents);
        } else {
            out.writeEncodingDL(withTag, contents, last, lastOctetDER);
        }
    }

    ASN1Primitive toDERObject() {
        return this;
    }

    ASN1Primitive toDLObject() {
        return this;
    }
}
