package threads.lite.cert;

class RuntimeOperatorException extends RuntimeException {
    private final Throwable cause;

    RuntimeOperatorException(String msg, Throwable cause) {
        super(msg);

        this.cause = cause;
    }

    public Throwable getCause() {
        return cause;
    }
}
