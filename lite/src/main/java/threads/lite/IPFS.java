package threads.lite;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.protobuf.ByteString;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ConnectException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

import crypto.pb.Crypto;
import merkledag.pb.Merkledag;
import record.pb.EnvelopeOuterClass;
import record.pb.RecordOuterClass;
import threads.lite.cid.Block;
import threads.lite.cid.Cid;
import threads.lite.cid.Dir;
import threads.lite.cid.IPV;
import threads.lite.cid.Multiaddr;
import threads.lite.cid.Multiaddrs;
import threads.lite.cid.Peer;
import threads.lite.cid.PeerId;
import threads.lite.cid.Record;
import threads.lite.core.AutonatResult;
import threads.lite.core.BlockStore;
import threads.lite.core.Cancellable;
import threads.lite.core.Keys;
import threads.lite.core.Link;
import threads.lite.core.Page;
import threads.lite.core.PageStore;
import threads.lite.core.PeerInfo;
import threads.lite.core.Progress;
import threads.lite.core.ProgressStream;
import threads.lite.core.Reader;
import threads.lite.core.ReaderInputStream;
import threads.lite.core.ReaderStream;
import threads.lite.core.Reservation;
import threads.lite.core.Resolver;
import threads.lite.core.Server;
import threads.lite.core.Session;
import threads.lite.crypto.Key;
import threads.lite.crypto.PubKey;
import threads.lite.dag.DagResolver;
import threads.lite.dag.DagStream;
import threads.lite.dht.DhtService;
import threads.lite.host.LiteHost;
import threads.lite.host.LitePush;
import threads.lite.host.LiteService;
import threads.lite.mdns.MDNS;
import threads.lite.quic.Connection;
import threads.lite.quic.Parameters;
import threads.lite.store.BLOCKS;
import threads.lite.store.PAGES;
import threads.lite.store.PEERS;
import threads.lite.store.SWARM;

/**
 * @noinspection WeakerAccess
 */
public final class IPFS {
    public static final String TIME_FORMAT_IPFS = "yyyy-MM-dd'T'HH:mm:ss.SSSSSSS'Z'";  // RFC3339Nano = "2006-01-02T15:04:05.999999999Z07:00"
    public static final String HOLE_PUNCH_PROTOCOL = "/libp2p/dcutr";
    public static final String RELAY_PROTOCOL_HOP = "/libp2p/circuit/relay/0.2.0/hop";
    public static final String RELAY_PROTOCOL_STOP = "/libp2p/circuit/relay/0.2.0/stop";
    public static final String DHT_PROTOCOL = "/ipfs/kad/1.0.0";
    public static final String LITE_PUSH_PROTOCOL = "/lite/push/1.0.0";
    public static final String LITE_PULL_PROTOCOL = "/lite/pull/1.0.0";
    public static final String LITE_SWAP_PROTOCOL = "/lite/swap/1.0.0";
    public static final String AUTONAT_PROTOCOL = "/libp2p/autonat/1.0.0";

    public static final String MULTISTREAM_PROTOCOL = "/multistream/1.0.0";

    public static final String BITSWAP_PROTOCOL = "/ipfs/bitswap/1.2.0";
    public static final String IDENTITY_PROTOCOL = "/ipfs/id/1.0.0";

    public static final String IDENTITY_PUSH_PROTOCOL = "/ipfs/id/push/1.0.0";
    public static final String INDEX_HTML = "index.html";
    public static final String AGENT = "lite/0.9.0/";
    public static final String PROTOCOL_VERSION = "ipfs/0.1.0";
    public static final String IPFS_PATH = "/ipfs/";
    public static final String IPNS_PATH = "/ipns/";
    public static final String KAD_BOOTSTRAP = "/ip4/104.131.131.82/udp/4001/quic-v1/p2p/QmaCpDMGvV2BGHeYERUEnRQAwe3N8SzbUtfsmvsqQLuvuJ"; // mars.i.ipfs.io
    public static final String NA = "na";
    public static final int CHUNK_SIZE = 262144;
    public static final int GRACE_PERIOD = 15;

    public static final String MDNS_SERVICE = "_p2p._udp."; // default libp2p [or kubo] service name

    @NonNull
    public static final List<String> DHT_BOOTSTRAP_NODES = Arrays.asList(
            "/dnsaddr/bootstrap.libp2p.io/p2p/QmNnooDu7bfjPFoTZYxMNLWUQJyrVwtbZg5gBMjTezGAJN",
            "/dnsaddr/bootstrap.libp2p.io/p2p/QmQCU2EcMqAqQPR2i9bChDtGNJchTbq5TbXJJ16u19uLTa",
            "/dnsaddr/bootstrap.libp2p.io/p2p/QmbLHAnMoJPWSCR5Zhtx6BHJX9KiKNN6tpvbUcqanj75Nb",
            "/dnsaddr/bootstrap.libp2p.io/p2p/QmcZf59bWwK5XFi76CZX8cbJ4BhTzzA3gU1ZjYZcYW3dwt"
    );
    public static final int REACHABLE_TIMEOUT = 250; // in ms
    public static final int DHT_TABLE_SIZE = 200;
    public static final int DHT_ALPHA = 50;
    public static final int DHT_CONCURRENCY = 5;
    public static final int DHT_REQUEST_TIMEOUT = 15; // in seconds
    public static final int DHT_MESSAGE_TIMEOUT = 5; // in seconds

    public static final int DEFAULT_REQUEST_TIMEOUT = 1; // in seconds
    public static final int AUTONAT_TIMEOUT = 10; // in seconds

    public static final int CONNECT_TIMEOUT = 5; // in seconds
    public static final int RELAY_CONNECT_TIMEOUT = 30; // in seconds
    public static final int IPNS_DURATION = 6; // 6 hours duration
    public static final int BITSWAP_REQUEST_DELAY = 2; // 2 sec
    @NonNull
    public static final Duration RECORD_EOL = Duration.ofHours(24);
    // TODO [Future high] should be removed, when virtual threads are available
    public static final long MAX_TIMEOUT = 60 * 60; // 1 hour = 3600 sec
    public static final String PROTOCOLS = "protocols";
    public static final String ALPNS = "alpns";

    private static final String IPFS_KEY = "IPFS_KEY";
    private static final String PRIVATE_KEY = "PRIVATE_KEY";
    private static final String PUBLIC_KEY = "PUBLIC_KEY";
    private static final String TAG = IPFS.class.getSimpleName();
    private static volatile IPFS INSTANCE = null;
    @NonNull
    private final LiteHost host;


    private IPFS(@NonNull Context context) throws Exception {
        this.host = new LiteHost(getKeys(context), BLOCKS.getInstance(context),
                PEERS.getInstance(context), SWARM.getInstance(context),
                PAGES.getInstance(context));
    }


    public static void setPublicKey(@NonNull Context context, @NonNull String key) {
        SharedPreferences sharedPref = context.getSharedPreferences(
                IPFS_KEY, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(PUBLIC_KEY, key);
        editor.apply();
    }

    @SuppressWarnings("UnusedReturnValue")
    public static long copy(InputStream source, OutputStream sink) throws IOException {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            return source.transferTo(sink);
        } else {
            long nread = 0L;
            byte[] buf = new byte[4096];
            int n;
            while ((n = source.read(buf)) > 0) {
                sink.write(buf, 0, n);
                nread += n;
            }
            return nread;
        }
    }

    public static void copy(@NonNull InputStream source, @NonNull OutputStream sink,
                            @NonNull Progress progress, long size) throws IOException {
        long nread = 0L;
        byte[] buf = new byte[4096];
        int remember = 0;
        int n;
        while ((n = source.read(buf)) > 0) {
            sink.write(buf, 0, n);
            nread += n;

            if (progress.doProgress()) {
                if (size > 0) {
                    int percent = (int) ((nread * 100.0f) / size);
                    if (percent > remember) {
                        remember = percent;
                        progress.setProgress(percent);
                    }
                }
            }
        }
    }

    @NonNull
    private static String getPublicKey(@NonNull Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences(
                IPFS_KEY, Context.MODE_PRIVATE);
        return Objects.requireNonNull(sharedPref.getString(PUBLIC_KEY, ""));

    }

    public static void setPrivateKey(@NonNull Context context, @NonNull String key) {
        SharedPreferences sharedPref = context.getSharedPreferences(
                IPFS_KEY, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(PRIVATE_KEY, key);
        editor.apply();
    }

    @NonNull
    private static String getPrivateKey(@NonNull Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences(
                IPFS_KEY, Context.MODE_PRIVATE);
        return Objects.requireNonNull(sharedPref.getString(PRIVATE_KEY, ""));

    }

    @NonNull
    public static IPFS getInstance(@NonNull Context context) throws Exception {
        if (INSTANCE == null) {
            synchronized (IPFS.class) {
                if (INSTANCE == null) {
                    INSTANCE = new IPFS(context);
                }
            }
        }
        return INSTANCE;
    }


    public static void push(@NonNull Connection connection,
                            @NonNull EnvelopeOuterClass.Envelope envelope) throws Exception {
        LiteService.push(connection, envelope);
    }

    @NonNull
    public static EnvelopeOuterClass.Envelope createEnvelope(@NonNull Session session,
                                                             @NonNull Record record,
                                                             byte[] payload) throws Exception {

        byte[] data = Record.unsignedEnvelopePayload(Record.PEER, payload);
        byte[] signature = Key.sign(session.getPrivateKey(), data);

        return EnvelopeOuterClass.Envelope.newBuilder()
                .setPublicKey(session.getPublicKey())
                .setPayload(ByteString.copyFrom(payload))
                .setPayloadType(ByteString.copyFrom(record.type()))
                .setSignature(ByteString.copyFrom(signature))
                .build();

    }

    public static void verifyEnvelope(Record record, EnvelopeOuterClass.Envelope envelope) throws Exception {
        PubKey pubKey = Key.unmarshalPublicKey(
                envelope.getPublicKey().toByteArray());

        pubKey.verify(Record.unsignedEnvelopePayload(
                        record, envelope.getPayload().toByteArray()),
                envelope.getSignature().toByteArray());
    }

    @NonNull
    public static String getText(@NonNull Session session, @NonNull Cid cid, @NonNull Progress progress)
            throws Exception {

        try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
            fetchToOutputStream(session, outputStream, cid, progress);
            return outputStream.toString();
        }
    }

    @NonNull
    public static byte[] getData(@NonNull Session session, @NonNull Cid cid, @NonNull Progress progress)
            throws Exception {

        try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
            fetchToOutputStream(session, outputStream, cid, progress);
            return outputStream.toByteArray();
        }
    }

    @NonNull
    public static byte[] getData(@NonNull Session session, @NonNull Cid cid, @NonNull Cancellable cancellable)
            throws Exception {

        try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
            fetchToOutputStream(session, outputStream, cid, cancellable);
            return outputStream.toByteArray();
        }
    }

    @NonNull
    public static Reader getReader(@NonNull Session session, @NonNull Cid cid,
                                   @NonNull Cancellable cancellable) throws Exception {
        return Reader.createReader(cancellable, session, cid);
    }

    // fetch (stores) the given cid into the output stream
    // Note: the data is automatically stored also in the block store
    public static void fetchToOutputStream(@NonNull Session session,
                                           @NonNull OutputStream outputStream,
                                           @NonNull Cid cid,
                                           @NonNull Progress progress)
            throws Exception {

        long totalRead = 0L;
        int remember = 0;

        Reader reader = getReader(session, cid, progress);
        long size = reader.getSize();

        do {
            if (progress.isCancelled()) {
                throw new InterruptedException("cancelled");
            }

            ByteString buffer = reader.loadNextData();
            if (buffer.isEmpty()) {
                return;
            }
            outputStream.write(buffer.toByteArray());

            // calculate progress
            totalRead += buffer.size();
            if (progress.doProgress()) {
                if (size > 0) {
                    int percent = (int) ((totalRead * 100.0f) / size);
                    if (remember < percent) {
                        remember = percent;
                        progress.setProgress(percent);
                    }
                }
            }
        } while (true);
    }

    // fetch (stores) the given cid into the output stream
    // Note: the data is automatically stored also in the block store
    public static void fetchToOutputStream(@NonNull Session session,
                                           @NonNull OutputStream outputStream,
                                           @NonNull Cid cid,
                                           @NonNull Cancellable cancellable)
            throws Exception {

        Reader reader = getReader(session, cid, cancellable);

        do {
            ByteString buffer = reader.loadNextData();
            if (buffer.isEmpty()) {
                return;
            }
            outputStream.write(buffer.toByteArray());
        } while (true);
    }

    @NonNull
    private static Keys getKeys(@NonNull Context context) throws Exception {

        if (!getPrivateKey(context).isEmpty() && !getPublicKey(context).isEmpty()) {

            Base64.Decoder decoder = Base64.getDecoder();

            byte[] publicKey = decoder.decode(getPublicKey(context));
            byte[] privateKey = decoder.decode(getPrivateKey(context));

            return new Keys(publicKey, privateKey);

        } else {
            Keys keys = Key.generateKeys();
            Base64.Encoder encoder = Base64.getEncoder();
            setPrivateKey(context, encoder.encodeToString(keys.privateKey()));
            setPublicKey(context, encoder.encodeToString(keys.publicKey()));
            return keys;
        }
    }

    @NonNull
    public static PeerInfo identity(@NonNull Server server) throws Exception {
        return server.identity();
    }

    @NonNull
    public static Multiaddrs dialableAddresses(@NonNull Server server) {
        return server.dialableAddresses();
    }

    @NonNull
    public static Page pull(@NonNull Connection connection) throws Exception {
        return LiteService.pull(connection);
    }

    @NonNull
    public static Cid storeFile(@NonNull Session session, @NonNull File file) throws Exception {
        try (FileInputStream inputStream = new FileInputStream(file)) {
            return storeInputStream(session, inputStream);
        }
    }

    @NonNull
    public static Cid storeData(@NonNull Session session, byte[] data) throws Exception {

        try (InputStream inputStream = new ByteArrayInputStream(data)) {
            return storeInputStream(session, inputStream);
        }
    }

    @NonNull
    public static Cid storeText(@NonNull Session session, @NonNull String text) throws Exception {

        try (InputStream inputStream = new ByteArrayInputStream(text.getBytes())) {
            return storeInputStream(session, inputStream);
        }
    }

    @NonNull
    public static Cid storeInputStream(@NonNull Session session, @NonNull InputStream inputStream,
                                       @NonNull Progress progress, long size) throws Exception {

        return DagStream.readInputStream(session,
                new ReaderInputStream(inputStream, progress, size));

    }

    @NonNull
    public static Cid storeInputStream(@NonNull Session session, @NonNull InputStream inputStream)
            throws Exception {
        return DagStream.readInputStream(session,
                new ReaderInputStream(inputStream, 0));
    }

    @NonNull
    public static String getText(@NonNull Session session, @NonNull Cid cid,
                                 @NonNull Cancellable cancellable) throws Exception {

        try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
            fetchToOutputStream(session, outputStream, cid, cancellable);
            return outputStream.toString();
        }
    }

    @NonNull
    public static InputStream getInputStream(@NonNull Session session, @NonNull Cid cid,
                                             @NonNull Cancellable cancellable)
            throws Exception {
        Reader reader = getReader(session, cid, cancellable);
        return new ReaderStream(reader);
    }

    @NonNull
    public static InputStream getInputStream(@NonNull Session session, @NonNull Cid cid,
                                             @NonNull Progress progress)
            throws Exception {
        Reader reader = getReader(session, cid, progress);
        return new ProgressStream(reader, progress);

    }

    public static void fetchToFile(@NonNull Session session, @NonNull File file,
                                   @NonNull Cid cid, @NonNull Cancellable cancellable)
            throws Exception {

        try (FileOutputStream fileOutputStream = new FileOutputStream(file)) {
            fetchToOutputStream(session, fileOutputStream, cid, cancellable);
        }
    }

    public static void fetchToFile(@NonNull Session session, @NonNull File file,
                                   @NonNull Cid cid, @NonNull Progress progress)
            throws Exception {

        try (FileOutputStream fileOutputStream = new FileOutputStream(file)) {
            fetchToOutputStream(session, fileOutputStream, cid, progress);
        }
    }

    // has the session block storage the cid block
    public static boolean hasBlock(@NonNull Session session, @NonNull Cid cid) {
        return session.getBlockStore().hasBlock(cid);
    }


    // remove the cid block (add all links blocks recursively) from the session block storage
    public static void removeBlocks(@NonNull Session session, @NonNull Cid cid) {
        DagStream.removeBlocks(session, cid);
    }

    // returns all blocks of the cid from the session block storage,
    // If the cid block contains links, also the links cid blocks are returned (recursive)
    @NonNull
    public static List<Cid> getBlocks(@NonNull Session session, @NonNull Cid cid) {
        return DagStream.getBlocks(session, cid);
    }

    // removes a link with the given name from the directory
    @NonNull
    public static Dir removeFromDirectory(@NonNull Session session, @NonNull Dir dir,
                                          @NonNull String name) throws Exception {
        return DagStream.removeFromDirectory(session, dir.cid(), name);
    }

    // add a link to to the directory (link should be of Type File, but not tested)
    @NonNull
    public static Dir addLinkToDirectory(@NonNull Session session, @NonNull Dir dir,
                                         @NonNull Link link) throws Exception {
        return DagStream.addLinkToDirectory(session, dir.cid(), link);
    }

    @NonNull
    public static Dir updateLinkToDirectory(@NonNull Session session, @NonNull Dir dir,
                                            @NonNull Link link) throws Exception {
        return DagStream.updateLinkToDirectory(session, dir.cid(), link);
    }

    // creates a directory with the given links  (links should be of Type File,
    // but it will not be checked within the API)
    @NonNull
    public static Dir createDirectory(@NonNull Session session, @NonNull List<Link> links)
            throws Exception {
        return DagStream.createDirectory(session, links);
    }

    @NonNull
    public static Dir createEmptyDirectory(@NonNull Session session) throws Exception {
        return DagStream.createEmptyDirectory(session);
    }

    // function requires a cancellable, because the cid could be remote
    public static boolean isDir(@NonNull Session session, @NonNull Cid cid, @NonNull Cancellable cancellable)
            throws Exception {
        return DagStream.isDir(cancellable, session, cid);
    }

    // function requires a cancellable, because the cid could be remote
    public static boolean hasLink(@NonNull Session session, @NonNull Cid cid,
                                  @NonNull String name, @NonNull Cancellable cancellable)
            throws Exception {
        return DagStream.hasLink(cancellable, session, cid, name);
    }

    // function requires a cancellable, because the cid could be remote
    // this function return when all links have been evaluated
    // Note: only links are returned when they have a name (usually a file and directory)
    // Note: when resolveChildren is true, the link node will be resolved and the type is known
    // Note: This function does not go recursive, only direct links are evaluated
    @NonNull
    public static List<Link> links(@NonNull Session session, @NonNull Cid cid, boolean resolveChildren,
                                   @NonNull Cancellable cancellable) throws Exception {

        List<Link> result = new ArrayList<>();
        links(session, cid, result::add, resolveChildren, cancellable);
        return result;
    }

    // function requires a cancellable, because the cid could be remote
    // this function present immediately links, when evaluated
    // Note: only links are returned when they have a name (usually a file and directory)
    // Note: when resolveChildren is true, the link node will be resolved and the type is known
    // Note: This function does not go recursive, only direct links are evaluated
    public static void links(@NonNull Session session, @NonNull Cid cid, @NonNull Consumer<Link> consumer,
                             boolean resolveChildren, @NonNull Cancellable cancellable)
            throws Exception {

        DagStream.ls(cancellable, link -> {
            if (!link.name().isEmpty()) {
                consumer.accept(link);
            }
        }, session, cid, resolveChildren);
    }

    // function requires a cancellable, because the cid could be remote
    // return all links, also of type raw and unknown
    // Note: when resolveChildren is true, the link node will be resolved and the type is known
    // Note: This function does not go recursive, only direct links are evaluated
    @NonNull
    public static List<Link> allLinks(@NonNull Session session, @NonNull Cid cid, boolean resolveChildren,
                                      @NonNull Cancellable cancellable) throws Exception {

        List<Link> links = new ArrayList<>();
        DagStream.ls(cancellable, links::add, session, cid, resolveChildren);
        return links;
    }

    @Nullable
    public static Page resolve(Session session, PeerId peerId, long sequence, Cancellable cancellable) {

        // TODO [Future urgent] replace by virtual threads
        ExecutorService service = Executors.newFixedThreadPool(2);
        AtomicReference<Page> found = new AtomicReference<>(null);
        service.execute(() -> {
            try {
                found.set(directResolvePage(session, peerId, getConnectionParameters(),
                        () -> cancellable.isCancelled() || found.get() != null));
                service.shutdownNow();
            } catch (Throwable throwable) {
                // this exception should happens quite often [only Ed25519 is supported and
                // lots of peerId are not dialable, because they are just made-up]
                LogUtils.error(TAG, throwable.getMessage());
            }
        });

        service.execute(() -> {
            try {
                Page entry = resolvePage(session, peerId, sequence,
                        () -> cancellable.isCancelled() || found.get() != null);
                if (entry != null) {
                    found.set(entry);
                    service.shutdownNow();
                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable.getMessage());
            }
        });

        service.shutdown();
        try {
            boolean termination = service.awaitTermination(cancellable.timeout(), TimeUnit.SECONDS);
            if (!termination) {
                service.shutdownNow();
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }


        return found.get();
    }

    // special function looks for the peerId and makes a direct connection
    // and then asks to retrieve the ipns entry
    @NonNull
    public static Page directResolvePage(Session session, PeerId peerId, Parameters parameters,
                                         Cancellable cancellable) throws Exception {
        PubKey pubKey = Key.extractPublicKey(peerId);
        if (pubKey.getKeyType() == Crypto.KeyType.Ed25519) {
            Connection connection = dial(session, peerId, parameters, cancellable);
            return LiteService.pull(connection);
        }
        throw new Exception("Direct resolve page not possible");
    }

    @Nullable
    public static Page resolvePage(@NonNull Session session, @NonNull PeerId peerId,
                                   long lastSequence, @NonNull Cancellable cancellable) {

        AtomicReference<Page> done = new AtomicReference<>();

        AtomicBoolean found = new AtomicBoolean(false);
        session.searchValue(() -> (found.get() || cancellable.isCancelled()),
                entry -> {
                    long sequence = entry.sequence();
                    if (sequence >= lastSequence) {
                        done.set(entry);
                        found.set(true);
                    }
                }, createIpnsKey(peerId));

        return done.get();
    }

    public static void provide(@NonNull Server server, @NonNull Connection connection,
                               @NonNull Cid cid) throws ExecutionException,
            InterruptedException, TimeoutException {
        DhtService.provide(server, connection, cid);
    }

    public static void provide(@NonNull Server server, @NonNull Cid cid,
                               @NonNull Consumer<Multiaddr> consumer, @NonNull Cancellable cancellable) {
        server.provide(cid, consumer, cancellable);
    }

    public static Set<PeerId> getProvider(Connection connection, Cid cid) {
        return DhtService.getProvider(connection, cid);
    }

    public static void findProviders(@NonNull Session session,
                                     @NonNull Consumer<Multiaddr> consumer,
                                     @NonNull Cid cid,
                                     @NonNull Cancellable cancellable) {
        session.findProviders(cancellable, consumer, cid);
    }

    public static void findPeer(@NonNull Session session, @NonNull PeerId peerId,
                                @NonNull Consumer<Multiaddr> consumer, @NonNull Cancellable cancellable) {
        session.findPeer(cancellable, consumer, peerId);
    }

    public static void findClosestPeers(@NonNull Session session, @NonNull PeerId peerId,
                                        @NonNull Consumer<Multiaddr> consumer,
                                        @NonNull Cancellable cancellable) {
        session.findClosestPeers(cancellable, consumer, peerId);
    }

    // no timeout is set, it is set internally
    // for normal connections it is IPFS.CONNECT_TIMEOUT 5 sec
    // for relayed connections it is IPFS.RELAY_CONNECT_TIMEOUT 30 sec (hole punch)
    // others like dns are somewhere in between
    @NonNull
    public static Connection dial(@NonNull Session session, @NonNull Multiaddr address,
                                  @NonNull Parameters parameters)
            throws ConnectException, InterruptedException, TimeoutException {
        return session.dial(address, parameters);
    }

    @NonNull
    public static Connection dial(@NonNull Session session, @NonNull PeerId peerId,
                                  @NonNull Parameters parameters, @NonNull Cancellable cancellable)
            throws InterruptedException {
        return session.dial(cancellable, peerId, parameters);
    }

    @NonNull
    public static Parameters getConnectionParameters() {
        return Parameters.getDefault();
    }

    @NonNull
    public static AutonatResult autonat(@NonNull Server server) {
        return server.autonat();
    }

    // create an ipns key has the form of "/ipns/"+ <hash of peerId>
    public static byte[] createIpnsKey(@NonNull PeerId peerId) {
        return Key.createIpnsKey(peerId);
    }

    // decodes an ipns key has of the form of "/ipns/"+ <hash of peerId> to the peerId
    @NonNull
    public static PeerId decodeIpnsKey(byte[] ipnsKey) throws Exception {
        return Key.decodeIpnsKey(ipnsKey);
    }

    // Note: this is usually the data of an ipns entry
    // default : "/ipfs/ +<encoded cid>
    @NonNull
    public static Cid decodeIpnsData(@NonNull Page page) throws Exception {
        return Page.decodeIpnsData(page.value());
    }

    @NonNull
    public static Cid decodeCid(@NonNull String cid) throws Exception {
        return Cid.decode(cid);
    }

    @NonNull
    public static PeerId decodePeerId(@NonNull String pid) throws Exception {
        return PeerId.decode(pid);
    }

    // Utility function, resolves a root Cid object till the path of links is reached
    @NonNull
    public static Cid resolveCid(@NonNull Session session, @NonNull Cid root, @NonNull List<String> path,
                                 @NonNull Cancellable cancellable) throws Exception {
        return DagResolver.resolveNode(cancellable, session, root, path);
    }

    @NonNull
    public static Multiaddr decodeMultiaddr(@NonNull String address) throws Exception {
        return Multiaddr.create(address);
    }

    // return true, when it has reservations
    public static boolean hasReservations(@NonNull Server server) {
        return server.hasReservations();
    }

    // this function returns all the valid reservations
    @NonNull
    public static Set<Reservation> reservations(@NonNull Server server) {
        return server.reservations();
    }

    // this function connects to the closest peer of ourself (called swarm)
    // the number of connections can be evaluated by the server.swarm() function
    public static void swarm(Server server, Cancellable cancellable) {
        server.swarm(cancellable);
    }

    // this function does the reservation [it is bound to a server]
    // Note: only reservation of version 2 is supported
    // https://github.com/libp2p/specs/blob/master/relay/circuit-v2.md#introduction
    // static relays are marked as Kind.STATIC, where limited relays are marked
    // as Kind.LIMITED (in the Reservation class)
    @NonNull
    public static Set<Reservation> reservations(@NonNull Server server, Cancellable cancellable) {
        server.reservations(cancellable);
        return server.reservations();
    }

    // this function does the reservation [it is bound to a server]
    // Note: only reservation of version 2 is supported
    // https://github.com/libp2p/specs/blob/master/relay/circuit-v2.md#introduction
    // static relays are marked as Kind.STATIC, where limited relays are marked
    // as Kind.LIMITED (in the Reservation class)
    // Note: this function is used for testing (though it still adds it to the internal
    // reservation list, when succeeds)
    @NonNull
    public static Reservation reservation(@NonNull Server server,
                                          @NonNull Multiaddr multiaddr) throws Exception {
        return server.reservation(multiaddr);
    }

    // returns next reservation cycle in minutes, after the minutes have past
    // the reservation process has to be done again, due to the fact that some or at least one
    // reservation expire time comes to an end
    // it works on the local stored reservations
    // Note: static reservations are not considered, only limited
    public static long nextReservationCycle(@NonNull Server server) {
        long nextCycle = 0;
        for (Reservation reservation : server.reservations()) {
            if (reservation.limit().limited()) {
                long expireInMinutes = reservation.expireInMinutes();
                if (nextCycle == 0) {
                    nextCycle = expireInMinutes;
                } else {
                    nextCycle = Math.min(nextCycle, expireInMinutes);
                }
            }
        }
        return nextCycle;
    }

    // https://developer.android.com/training/connect-devices-wirelessly/nsd
    @NonNull
    public static MDNS mdns(Context context) {
        return mdns(context, IPFS.MDNS_SERVICE);
    }

    @NonNull
    public static MDNS mdns(Context context, String mdnsName) {
        return MDNS.create(context, mdnsName);
    }

    @NonNull
    public static String resolveDnsLink(@NonNull String hostName) {
        return Resolver.resolveDnsLink(hostName);
    }

    @NonNull
    public Keys getKeys() {
        return host.getKeys();
    }

    @NonNull
    public PeerId self() {
        return host.self();
    }

    @NonNull
    public PeerInfo getPeerInfo(@NonNull Connection connection) throws Exception {
        return host.getPeerInfo(connection);
    }

    public void setIncomingPush(@Nullable Consumer<LitePush> incomingPush) {
        this.host.setIncomingPush(incomingPush);
    }

    @NonNull
    public Session createSession(@NonNull BlockStore blockStore) {
        return createSession(blockStore, cid -> true);
    }

    @NonNull
    public Session createSession(@NonNull BlockStore blockStore,
                                 @NonNull Function<Cid, Boolean> isBitswapActive) {
        return host.createSession(blockStore, isBitswapActive);
    }

    @NonNull
    public Session createSession(@NonNull Function<Cid, Boolean> isBitswapActive) {
        return host.createSession(getBlockStore(), isBitswapActive);
    }

    @NonNull
    public Session createSession() {
        return createSession(getBlockStore(), cid -> true);
    }

    public void publishName(@NonNull Session session, @NonNull Cid cid,
                            @Nullable String name, @NonNull Consumer<Multiaddr> consumer,
                            @NonNull Cancellable cancellable) throws Exception {
        publishName(session, Page.encodeIpnsData(cid), name, consumer, cancellable);
    }

    public void publishName(@NonNull Session session, byte[] value,
                            @Nullable String name, @NonNull Consumer<Multiaddr> consumer,
                            @NonNull Cancellable cancellable) throws Exception {
        RecordOuterClass.Record ipnsRecord = createSelfSignedIpnsRecord(value, name);
        session.putValue(cancellable, consumer, ipnsRecord);
    }

    public boolean publishName(@NonNull Connection connection, @NonNull Cid cid,
                               @Nullable String name) throws Exception {
        RecordOuterClass.Record ipnsRecord = createSelfSignedIpnsRecord(
                Page.encodeIpnsData(cid), name);
        return DhtService.putValue(connection, ipnsRecord);
    }

    @SuppressWarnings("unused")
    public void provideRecursively(@NonNull Server server, @NonNull Cid cid,
                                   @NonNull Cancellable cancellable) {
        server.provide(cid, multiaddr -> {
        }, cancellable);

        Block block = getBlockStore().getBlock(cid);
        if (block != null) { // block is locally available
            Merkledag.PBNode node = block.node();
            List<Merkledag.PBLink> links = node.getLinksList();

            for (Merkledag.PBLink link : links) {
                Cid child = Cid.decode(link.getHash().toByteArray());
                provideRecursively(server, child, cancellable);
            }
        }
    }

    @NonNull
    public Server startServer(int port,
                              @NonNull Consumer<Connection> connectConsumer,
                              @NonNull Consumer<Connection> closedConsumer,
                              @NonNull Function<PeerId, Boolean> isGated) {
        return host.startSever(port, connectConsumer, closedConsumer, isGated);
    }

    @NonNull
    public Set<Multiaddr> getBootstrap() {
        return host.getBootstrap();
    }

    public void setRecordSupplier(@Nullable Supplier<RecordOuterClass.Record> recordSupplier) {
        host.setRecordSupplier(recordSupplier);
    }

    // when using this function make sure, that the value might be "encoded" already
    // (see encodeIpnsData, for example, but maybe you do not need an encoding)
    // Note: sealed means, that remote peers can verify that you have made the record,
    // it does not mean that the content itself is encrypted
    // Note: you always signed it with your own private key (signing with another own private key
    // is not supported)
    @NonNull
    public RecordOuterClass.Record createSelfSignedIpnsRecord(byte[] value, @Nullable String name) throws Exception {
        @SuppressLint("SimpleDateFormat") String format = new SimpleDateFormat(
                IPFS.TIME_FORMAT_IPFS).format(new Date());
        byte[] sealedRecord = host.createSelfSignedRecord(value, Page.getDefaultEol(), name);
        return RecordOuterClass.Record.newBuilder()
                .setKey(ByteString.copyFrom(Key.createIpnsKey(self())))
                .setValue(ByteString.copyFrom(sealedRecord))
                .setTimeReceived(format).build();
    }

    // Utility function, to resolve a dnsaddr [Address starts with "/dnsaddr/"]
    @NonNull
    public Set<Multiaddr> resolveDnsaddr(@NonNull Multiaddr multiaddr) throws Exception {
        if (!multiaddr.isDnsaddr()) {
            throw new Exception("not a dnsaddr");
        }
        return Resolver.resolveDnsaddr(host.ipv().get(), multiaddr);
    }

    @NonNull
    public BlockStore getBlockStore() {
        return host.getBlockStore();
    }

    public Supplier<IPV> ipv() {
        return host.ipv();
    }

    @NonNull
    public Set<Peer> getRoutingPeers() {
        return host.getRoutingPeers();
    }

    @NonNull
    public PageStore getPageStore() {
        return host.getPageStore();
    }

    public void updateNetwork() {
        host.updateNetwork();
    }


}
