package threads.lite.quic;

import androidx.annotation.NonNull;

interface PacketReceived {

    static boolean anyMatch(PacketReceived packetReceived, FrameType frameType) {
        for (FrameReceived frameReceived : packetReceived.frames()) {
            if (frameReceived.type() == frameType) {
                return true;
            }
        }
        return false;
    }

    // https://tools.ietf.org/html/draft-ietf-quic-recovery-33#section-2
    // "Packets that contain ack-eliciting frames elicit an ACK from the receiver (...) and are called ack-eliciting packets."
    static boolean isAckEliciting(PacketReceived packetReceived) {
        for (FrameReceived frameReceived : packetReceived.frames()) {
            if (FrameReceived.isAckEliciting(frameReceived)) {
                return true;
            }
        }
        return false;
    }

    PacketHeader packetHeader();

    Keys updated();

    default Level level() {
        return packetHeader().level();
    }

    default Version version() {
        return packetHeader().version();
    }

    @NonNull
    FrameReceived[] frames();

    default byte[] destinationConnectionId() {
        return packetHeader().destinationConnectionId();
    }

    long packetNumber();

    boolean hasUpdatedKeys();

    record Handshake(PacketHeader packetHeader, FrameReceived[] frames,
                     long packetNumber) implements PacketReceived {
        @Override
        public Keys updated() {
            throw new IllegalStateException("VersionNegotiation has no updated keys");
        }

        @Override
        public boolean hasUpdatedKeys() {
            return false;
        }
    }


    record Initial(PacketHeader packetHeader, FrameReceived[] frames,
                   long packetNumber, byte[] token) implements PacketReceived {

        public byte[] sourceConnectionId() {
            return packetHeader().sourceConnectionId();
        }

        @Override
        public Keys updated() {
            throw new IllegalStateException("VersionNegotiation has no updated keys");
        }

        @Override
        public boolean hasUpdatedKeys() {
            return false;
        }
    }


    record ShortHeader(PacketHeader packetHeader,
                       FrameReceived[] frames,
                       long packetNumber, Keys updated) implements PacketReceived {

        @Override
        public boolean hasUpdatedKeys() {
            return updated != null;
        }
    }


}
