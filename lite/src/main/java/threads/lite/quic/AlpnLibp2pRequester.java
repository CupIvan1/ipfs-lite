package threads.lite.quic;

import androidx.annotation.NonNull;

import java.nio.ByteBuffer;
import java.util.Objects;
import java.util.function.Consumer;

import threads.lite.LogUtils;


public record AlpnLibp2pRequester(@NonNull StreamRequester streamRequester, @NonNull State state)
        implements Consumer<StreamData> {
    private static final String TAG = AlpnLibp2pRequester.class.getSimpleName();


    public static AlpnLibp2pRequester create(@NonNull StreamRequester streamRequester) {
        return new AlpnLibp2pRequester(streamRequester, new State(streamRequester));
    }

    private static boolean isProtocol(byte[] data) {
        if (data.length > 2) {
            if (data[0] == '/' && data[data.length - 1] == '\n') {
                return true;
            } else if (data[0] == 'n' && data[1] == 'a' && data[2] == '\n') {
                return true;
            } else return data[0] == 'l' && data[1] == 's' && data[2] == '\n';
        }
        return false;
    }

    private static byte[] unsignedVarintReader(ByteBuffer data) {
        Objects.requireNonNull(data);
        return new byte[readUnsignedVariant(data)];
    }

    private static int readUnsignedVariant(ByteBuffer in) {
        int result = 0;
        int cur;
        int count = 0;
        do {
            cur = in.get() & 0xff;
            result |= (cur & 0x7f) << (count * 7);
            count++;
        } while (((cur & 0x80) == 0x80) && count < 5);
        if ((cur & 0x80) == 0x80) {
            throw new IllegalStateException("invalid unsigned variant sequence");
        }
        return result;
    }

    private static void iteration(State state, Stream stream, ByteBuffer bytes) throws Exception {

        if (!state.reset) {
            if (state.length() == 0) {
                state.frame = unsignedVarintReader(bytes);
                state.frameIndex = 0;
                if (state.length() <= 0) {
                    state.frame = null;
                    state.frameIndex = 0;
                    throw new Exception("invalid length of <= 0");
                } else {

                    int read = Math.min(state.length(), bytes.remaining());
                    for (int i = 0; i < read; i++) {
                        state.frame[state.frameIndex] = bytes.get();
                        state.frameIndex++;
                    }

                    if (read == state.length()) {
                        byte[] frame = Objects.requireNonNull(state.frame);
                        state.frame = null;
                        state.accept(stream, frame);
                    }

                    // check for a next iteration
                    if (bytes.remaining() > 0) {
                        iteration(state, stream, bytes);
                    }
                }
            } else {
                byte[] frame = Objects.requireNonNull(state.frame);
                int remaining = state.frame.length - state.frameIndex;
                int read = Math.min(remaining, bytes.remaining());
                for (int i = 0; i < read; i++) {
                    state.frame[state.frameIndex] = bytes.get();
                    state.frameIndex++;
                }
                remaining = state.frame.length - state.frameIndex;
                if (remaining == 0) { // frame is full
                    state.frame = null;
                    state.frameIndex++;
                    state.accept(stream, frame);
                }
                // check for a next iteration
                if (bytes.remaining() > 0) {
                    iteration(state, stream, bytes);
                }
            }
        }
    }


    @Override
    public void accept(StreamData rawData) {

        Stream stream = rawData.stream();

        if (rawData.isTerminated()) {
            state.reset();
            streamRequester.terminated();
            return;
        }

        try {
            byte[] data = rawData.data();
            Objects.requireNonNull(data); // can not be null
            if (data.length > 0) {
                iteration(state, rawData.stream(), ByteBuffer.wrap(data));
            }
        } catch (Throwable throwable) {

            LogUtils.error(TAG, "stream requester exception " +
                    streamRequester.getClass().getSimpleName() + " " +
                    throwable.getClass().getSimpleName());

            state.reset();
            stream.terminate();
            streamRequester.throwable(throwable);
        }

        if (rawData.isFinal()) {
            streamRequester.fin(stream);
            state.reset();
        }
    }


    static class State {

        private final StreamRequester streamRequester;


        byte[] frame = null;
        int frameIndex = 0;
        boolean reset = false;

        State(StreamRequester streamRequester) {
            this.streamRequester = streamRequester;
        }

        void reset() {
            frame = null;
            reset = true;
        }

        public void accept(Stream stream, byte[] frame) throws Exception {
            if (isProtocol(frame)) {
                String protocol = new String(frame, 0, frame.length - 1);
                streamRequester.protocol(stream, protocol);
            } else {
                streamRequester.data(stream, frame);
            }
        }

        int length() {
            if (frame != null) {
                return frame.length;
            }
            return 0;
        }

        @NonNull
        @Override
        public String toString() {
            return "State{" + "frameIndex=" + frameIndex + ", reset=" + reset + '}';
        }
    }
}
