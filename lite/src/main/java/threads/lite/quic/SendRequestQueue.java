package threads.lite.quic;


import android.os.Build;

import androidx.annotation.Nullable;

import java.util.Deque;
import java.util.Iterator;
import java.util.Objects;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.function.Consumer;
import java.util.function.Function;


final class SendRequestQueue {


    private final Deque<SendRequest> requestQueue = new ConcurrentLinkedDeque<>();
    private final Deque<Frame[]> probeQueue = new ConcurrentLinkedDeque<>();


    void addRequest(Frame fixedFrame, Consumer<Frame> lostCallback) {
        requestQueue.addLast(new SendRequest(fixedFrame.frameLength(),
                actualMaxSize -> fixedFrame, lostCallback));
    }


    void addProbeRequest(Frame[] frames) {
        probeQueue.addLast(frames);
    }

    boolean hasProbe() {
        return !probeQueue.isEmpty();
    }

    boolean hasProbeWithData() {
        Frame[] firstProbe = probeQueue.peekFirst();
        return firstProbe != null && firstProbe.length > 0;
    }

    Frame[] getProbe() {
        Frame[] probe = probeQueue.pollFirst();
        // Even when client first checks for a probe, this might happen due to race condition with clear().
        // (and don't bother too much about the chance of an unnecessary probe)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            return Objects.requireNonNullElseGet(probe, () -> new Frame[]{Frame.PING});
        } else {
            //noinspection ReplaceNullCheck
            if (probe != null) {
                return probe;
            } else {
                return new Frame[]{Frame.PING};
            }
        }
    }


    /**
     * @param estimatedSize The minimum size of the frame that the supplier can produce. When the supplier is
     *                      requested to produce a frame of that size, it must return a frame of the size or smaller.
     *                      This leaves room for the caller to handle uncertainty of how large the frame will be,
     *                      for example due to a var-length int value that may be larger at the moment the frame
     */
    void addRequest(Function<Integer, Frame> frameSupplier, int estimatedSize, Consumer<Frame> lostCallback) {
        requestQueue.addLast(new SendRequest(estimatedSize, frameSupplier, lostCallback));
    }

    boolean hasRequests() {
        return !requestQueue.isEmpty();
    }

    @Nullable
    public SendRequest next(int maxFrameLength) {
        if (maxFrameLength < 1) {  // Minimum frame size is 1: some frames (e.g. ping) are just a type field.
            // Forget it
            return null;
        }

        for (Iterator<SendRequest> iterator = requestQueue.iterator(); iterator.hasNext(); ) {
            SendRequest next = iterator.next();
            if (next.estimatedSize() <= maxFrameLength) {
                iterator.remove();
                return next;
            }
        }
        // Couldn't find one.
        return null;

    }

    public void clear() {
        requestQueue.clear();
        probeQueue.clear();
    }
}

