package threads.lite.quic;

public abstract class ApplicationProtocolConnectionFactory {

    public abstract void createConnection(String ignoredProtocol,
                                          ServerConnection connection) throws TransportError;
}
