package threads.lite.ident;

import androidx.annotation.NonNull;

import com.google.protobuf.ByteString;

import java.util.Arrays;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;

import crypto.pb.Crypto;
import identify.pb.IdentifyOuterClass;
import peer.pb.PeerRecordOuterClass;
import record.pb.EnvelopeOuterClass;
import threads.lite.IPFS;
import threads.lite.LogUtils;
import threads.lite.cid.Multiaddr;
import threads.lite.cid.Multiaddrs;
import threads.lite.cid.PeerId;
import threads.lite.cid.Record;
import threads.lite.core.DataHandler;
import threads.lite.core.PeerInfo;
import threads.lite.crypto.Key;
import threads.lite.crypto.PubKey;
import threads.lite.quic.Connection;
import threads.lite.quic.Stream;
import threads.lite.quic.StreamRequester;

public interface IdentityService {


    @NonNull
    static IdentifyOuterClass.Identify createOwnIdentity(
            @NonNull PeerId self, @NonNull byte[] privateKey, @NonNull Crypto.PublicKey publicKey,
            @NonNull Set<String> protocols, @NonNull Set<Multiaddr> multiaddrs) {


        IdentifyOuterClass.Identify.Builder builder = IdentifyOuterClass.Identify.newBuilder()
                .setAgentVersion(IPFS.AGENT)
                .setPublicKey(ByteString.copyFrom(publicKey.toByteArray()))
                .setProtocolVersion(IPFS.PROTOCOL_VERSION);


        if (!multiaddrs.isEmpty()) {
            for (Multiaddr addr : multiaddrs) {
                builder.addListenAddrs(ByteString.copyFrom(addr.address()));
            }
        }

        for (String protocol : protocols) {
            builder.addProtocols(protocol);
        }

        // currently not activated
        if (LogUtils.isDebug()) {
            try {
                // activate in the future when it is needed, also implement optimizations
                // - store envelope in host and update when the multiaddrs changed
                PeerRecordOuterClass.PeerRecord.Builder peerRecordBuilder
                        = PeerRecordOuterClass.PeerRecord.newBuilder();
                if (!multiaddrs.isEmpty()) {
                    for (Multiaddr addr : multiaddrs) {
                        PeerRecordOuterClass.PeerRecord.AddressInfo.Builder addressInfoBuilder =
                                PeerRecordOuterClass.PeerRecord.AddressInfo.newBuilder();
                        addressInfoBuilder.setMultiaddr(ByteString.copyFrom(addr.address()));
                        peerRecordBuilder.addAddresses(addressInfoBuilder.build());
                    }
                }
                peerRecordBuilder.setPeerId(ByteString.copyFrom(self.encoded()));
                peerRecordBuilder.setSeq(System.currentTimeMillis());
                byte[] payload = peerRecordBuilder.build().toByteArray();

                byte[] data = Record.unsignedEnvelopePayload(Record.PEER, payload);

                EnvelopeOuterClass.Envelope.Builder envelopeBuilder =
                        EnvelopeOuterClass.Envelope.newBuilder();

                envelopeBuilder.setPayloadType(ByteString.copyFrom(Record.PEER.type()));
                envelopeBuilder.setPublicKey(publicKey);
                envelopeBuilder.setPayload(ByteString.copyFrom(payload));
                envelopeBuilder.setSignature(ByteString.copyFrom(Key.sign(privateKey, data)));

                builder.setSignedPeerRecord(envelopeBuilder.build().toByteString());
            } catch (Throwable throwable) {
                throw new IllegalStateException(throwable);
            }
        }
        return builder.build();
    }

    @NonNull
    static PeerInfo getPeerInfo(PeerId self, Connection connection) throws Exception {
        IdentifyOuterClass.Identify identify = IdentityService.getIdentity(connection);
        return getPeerInfo(self, identify, connection.remotePeerId());
    }

    @NonNull
    static PeerInfo getPeerInfo(PeerId self, IdentifyOuterClass.Identify identify, PeerId expected)
            throws Exception {

        String agent = identify.getAgentVersion();


        PubKey pk = Key.unmarshalPublicKey(identify.getPublicKey().toByteArray());
        PeerId peerId = Key.fromPubKey(pk);

        if (!Objects.equals(peerId, expected)) {
            throw new Exception("PeerId is not what is expected");
        }

        Multiaddr observedAddr = Multiaddr.create(self, identify.getObservedAddr().toByteArray());


        String[] protocols = new String[identify.getProtocolsCount()];
        identify.getProtocolsList().toArray(protocols);


        Multiaddrs multiaddrs = Multiaddr.create(peerId, identify.getListenAddrsList());
        Multiaddr[] addresses = new Multiaddr[multiaddrs.size()];
        multiaddrs.toArray(addresses);

        try {
            // [check if it is necessary somewhere]
            if (LogUtils.isDebug()) {
                if (identify.hasSignedPeerRecord()) {
                    EnvelopeOuterClass.Envelope envelope =
                            EnvelopeOuterClass.Envelope.parseFrom(identify.getSignedPeerRecord());

                    LogUtils.debug(IdentityService.class.getSimpleName(), envelope.toString());

                    PeerRecordOuterClass.PeerRecord peerRecord =
                            PeerRecordOuterClass.PeerRecord.parseFrom(envelope.getPayload());

                    LogUtils.debug(IdentityService.class.getSimpleName(), peerRecord.toString());

                    for (PeerRecordOuterClass.PeerRecord.AddressInfo addressInfo :
                            peerRecord.getAddressesList()) {

                        Multiaddr multiaddr = Multiaddr.create(peerId,
                                addressInfo.getMultiaddr().toByteArray());
                        if (multiaddr != null) {
                            LogUtils.debug(IdentityService.class.getSimpleName(),
                                    multiaddr.toString());
                        }
                    }

                    byte[] payloadType = envelope.getPayloadType().toByteArray();

                    if (!Arrays.equals(payloadType, Record.PEER.type())) {
                        throw new Exception("Not expected payload type");
                    }

                    try {
                        PubKey pubKey = Key.unmarshalPublicKey(
                                envelope.getPublicKey().toByteArray());

                        PeerId peerIdRecord = Key.fromPubKey(pk);

                        if (!Objects.equals(peerIdRecord, expected)) {
                            throw new Exception("PeerId is not what is expected");
                        }

                        pubKey.verify(
                                Record.unsignedEnvelopePayload(Record.PEER,
                                        envelope.getPayload().toByteArray()),
                                envelope.getSignature().toByteArray());

                    } catch (Throwable throwable) {
                        LogUtils.error(IdentityService.class.getSimpleName(), throwable);
                    }
                }
            }
        } catch (Throwable throwable) {
            LogUtils.error(IdentityService.class.getSimpleName(), throwable);
        }

        return new PeerInfo(peerId, agent, addresses, protocols, observedAddr);
    }

    @NonNull
    private static IdentifyOuterClass.Identify getIdentity(
            @NonNull Connection connection) throws Exception {

        CompletableFuture<IdentifyOuterClass.Identify> done = new CompletableFuture<>();

        StreamRequester.createStream(connection, new IdentityRequest(done))
                .request(DataHandler.encodeProtocols(IPFS.MULTISTREAM_PROTOCOL,
                        IPFS.IDENTITY_PROTOCOL), IPFS.DEFAULT_REQUEST_TIMEOUT);

        return done.get(IPFS.DEFAULT_REQUEST_TIMEOUT, TimeUnit.SECONDS);

    }

    record IdentityRequest(
            CompletableFuture<IdentifyOuterClass.Identify> done) implements StreamRequester {
        @Override
        public void throwable(Throwable throwable) {
            done.completeExceptionally(throwable);
        }

        @Override
        public void protocol(Stream stream, String protocol) throws Exception {
            if (!Arrays.asList(IPFS.MULTISTREAM_PROTOCOL, IPFS.IDENTITY_PROTOCOL).contains(protocol)) {
                throw new Exception("Token " + protocol + " not supported");
            }
        }

        @Override
        public void data(Stream stream, byte[] data) throws Exception {
            done.complete(IdentifyOuterClass.Identify.parseFrom(data));
        }

        @Override
        public void terminated() {
            if (!done.isDone()) {
                done.completeExceptionally(new Throwable("stream terminated"));
            }
        }

        @Override
        public void fin(Stream stream) {
            // this is invoked, that is good, but request is finished when message is parsed [ok]
        }

    }
}
