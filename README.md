# IPFS Lite

IPFS Lite is an application to publish content via the IPFS/IPNS technology.

## General

The basic characteristics of the app are decentralized, respect of personal data,
open source, free of charge, transparent, free of advertising and legally impeccable.

[<img src="https://fdroid.gitlab.io/artwork/badge/get-it-on.png"
alt="Get it on F-Droid"
height="80">](https://f-droid.org/packages/threads.server/)
[<img src="https://play.google.com/intl/en_us/badges/images/generic/en-play-badge.png"
alt="Get it on Google Play"
height="80">](https://play.google.com/store/apps/details?id=threads.server)

## Documentation

**IPFS Lite** is a server only implementation. The related client application can be found at the
following location [Thor](https://gitlab.com/lp2p/thor).

The reason for the strict separation between client and server, relies in performance reasons.

The client **Thor** is available for several app stores (like F-Droid, Google Play).

**IPFS Lite** based on a subset of [IPFS](https://ipfs.io/), which is described in detail
in the [Lite Library](https://gitlab.com/lp2p/lite/)

### Tips

Due to the fact that **IPFS** itself does not offer static relays anymore, it is a common
problem that **IPFS Lite** is not reachable from the outside.

The application itself tries to connect to relays, so that at least it can be reachable from the
outside via **hole punching**, in case when it is not public reachable.

In case the application is not reachable, you might improve the reachability by switching
to **IPv6** or do **port forwarding** in case you are behind a local router.

**Hole punching** only will work, when your device is not behind a symmetric nat. In case
your are behind a symmetric nat, your application will only work in "LOCAL" mode,
which means it just works in the same local area network (LAN).

The reachability information will be presented to the user.

### Implementation

The current implementation **IPFS Lite** based entirely on the [IPNS](https://docs.ipfs.tech/concepts/ipns/#mutability-in-ipfs) idea.

Consequences and benefits of this approach
<ul>
  <li>Only the <strong>ipns</strong> link is shared, all changing data within the server are 
reflected automatically by the client. No additional steps required by the server.</li>
  <li>The <strong>ipns</strong> link published by the server is directly connected to its peer ID. 
That means, that a client can automatically find the server via the DHT, when it obtains the
<strong>ipns</strong> link.</li>
  <li>In order that clients can find the <strong>IPFS Lite</strong> server node via the DHT, 
the server has to connect to nodes near its peer ID (called swarm). Moreover when
the server is behind a NAT, it can connect directly to the relay nodes within its swarm.</li>
  <li>Shortcut for clients: Instead of doing a lookup in IPFS DHT for the <strong>ipns</strong> link
to receive the root CID object, it simply asks the server itself for the root CID object (which
contains the content information)</li>
</ul>

### Missing Features

The most important missing feature is, that **IPFS Lite** should support the relay feature as a
server, so that it can support other nodes to connect to each other. This feature should
only be available, when the node is global reachable and it can be turned off in the settings
by the user. Also in the first step, only limited relay should be supported not a static one.

Another feature (though technically is just an enabling) is, that the server should support
publishing content to the general IPFS DHT again. Precondition is that the server is global
reachable. Moreover only the IPNS entry and the associated root CID object should be published
due the fact of performance and memory consumption.

**Right now this feature is disabled, because IPFS itself keep the information in the DHT
only for a short period of time.**

To be more precise, it keeps the addresses of the CID entry only a short period of time.
The related Peer ID, will be stored for a longer period of time. In case of **IPFS Lite**
this behaviour is not useful.

### Conclusion

The current implementation of **IPFS Lite** is not really compatible to IPFS(kubo), due
to the fact that it does not publish any data on the IPFS DHT. Because simply it does not make
sense.

Therefore a regular IPFS node, will probably not find and **IPFS Lite** node data,because it does
not try to connect directly to the node which offers the **IPNS** data.

With this in mind the, a client (**Thor**) has been developed, which is optimized for
this behaviour. This client can also handle regular IPFS/IPNS data.

## Links

[Privacy Policy](https://gitlab.com/ipfs-lite/ipfs-lite/-/blob/master/POLICY.md)
<br/>
[Apache License](https://gitlab.com/ipfs-lite/ipfs-lite/-/blob/master/LICENSE)
