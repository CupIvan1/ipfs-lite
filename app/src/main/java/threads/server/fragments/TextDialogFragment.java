package threads.server.fragments;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;

import com.google.android.material.dialog.MaterialAlertDialogBuilder;

import java.util.Objects;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import threads.server.LogUtils;
import threads.server.R;
import threads.server.core.DOCS;
import threads.server.core.events.EVENTS;

@SuppressWarnings("WeakerAccess")
public class TextDialogFragment extends DialogFragment {
    public static final String TAG = TextDialogFragment.class.getSimpleName();

    private long mLastClickTime = 0;
    private TextView mTextEdit;

    public static TextDialogFragment newInstance(long parent) {
        Bundle bundle = new Bundle();
        bundle.putLong(DOCS.IDX, parent);
        TextDialogFragment fragment = new TextDialogFragment();
        fragment.setArguments(bundle);
        return fragment;
    }


    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        LayoutInflater inflater = getLayoutInflater();
        View view = inflater.inflate(R.layout.new_text, null);

        mTextEdit = view.findViewById(R.id.text_edit);
        Objects.requireNonNull(mTextEdit);

        Bundle bundle = getArguments();
        Objects.requireNonNull(bundle);

        long parent = bundle.getLong(DOCS.IDX);

        MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(requireContext());
        Dialog dialog = builder.setTitle(getString(R.string.new_text_file))
                .setView(view)
                .setNegativeButton(android.R.string.cancel, (d, id) -> dismiss())
                .setPositiveButton(android.R.string.ok, (d, id) -> {

                    try {
                        if (SystemClock.elapsedRealtime() - mLastClickTime < DOCS.CLICK_OFFSET) {
                            return;
                        }
                        mLastClickTime = SystemClock.elapsedRealtime();

                        removeKeyboards();

                        CharSequence text = mTextEdit.getText();
                        String content = "";
                        if (text != null) {
                            content = text.toString();
                            content = content.trim();
                        }

                        if (!content.isEmpty()) {
                            DOCS docs = DOCS.getInstance(requireContext());
                            ExecutorService executor = Executors.newSingleThreadExecutor();
                            String finalContent = content;
                            executor.execute(() -> docs.createTextFile(parent, finalContent));
                        } else {
                            EVENTS.getInstance(requireContext()).error(
                                    getString(R.string.empty_text));
                        }
                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                    } finally {
                        dismiss();
                    }
                }).create();
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        return dialog;
    }

    private void removeKeyboards() {

        try {
            InputMethodManager imm = (InputMethodManager)
                    requireContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            if (imm != null) {
                imm.hideSoftInputFromWindow(mTextEdit.getWindowToken(), 0);
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }

    }

    @Override
    public void onDismiss(@NonNull DialogInterface dialog) {
        super.onDismiss(dialog);
        removeKeyboards();
    }

}
