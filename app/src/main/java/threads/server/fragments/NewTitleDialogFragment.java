package threads.server.fragments;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.SystemClock;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;

import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.util.Objects;
import java.util.concurrent.atomic.AtomicBoolean;

import threads.server.InitApplication;
import threads.server.LogUtils;
import threads.server.R;
import threads.server.core.DOCS;
import threads.server.core.events.EVENTS;

public class NewTitleDialogFragment extends DialogFragment {
    public static final String TAG = NewTitleDialogFragment.class.getSimpleName();


    private final AtomicBoolean notPrintErrorMessages = new AtomicBoolean(false);
    private long mLastClickTime = 0;
    private TextInputEditText textInputEditText;
    private TextInputLayout textInputLayout;

    public static NewTitleDialogFragment newInstance() {
        return new NewTitleDialogFragment();
    }


    @Override
    public void onResume() {
        super.onResume();
        notPrintErrorMessages.set(true);
        isValidTitleName(getDialog());
        notPrintErrorMessages.set(false);
    }

    @Override
    @NonNull
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        LayoutInflater inflater = getLayoutInflater();
        MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(requireContext());

        View view = inflater.inflate(R.layout.new_title, null);
        textInputLayout = view.findViewById(R.id.title_input_layout);
        textInputLayout.setCounterEnabled(true);
        textInputLayout.setCounterMaxLength(50);


        textInputEditText = view.findViewById(R.id.title_text);
        InputFilter[] filterTitle = new InputFilter[1];
        filterTitle[0] = new InputFilter.LengthFilter(50);
        textInputEditText.setFilters(filterTitle);
        textInputEditText.setText(InitApplication.getTitle(requireContext()));
        textInputEditText.requestFocus();
        textInputEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                isValidTitleName(getDialog());
            }
        });


        builder.setView(view)
                // Add action buttons
                .setPositiveButton(android.R.string.ok, (dialog, id) -> {

                    if (SystemClock.elapsedRealtime() - mLastClickTime < DOCS.CLICK_OFFSET) {
                        return;
                    }

                    mLastClickTime = SystemClock.elapsedRealtime();

                    removeKeyboards();

                    Editable text = textInputEditText.getText();
                    Objects.requireNonNull(text);
                    String name = text.toString();

                    if (!Objects.equals(name, InitApplication.getTitle(requireContext()))) {
                        InitApplication.setTitle(requireContext(), name);
                        EVENTS.getInstance(requireContext()).title();
                    }
                })

                .setTitle(R.string.settings);


        return builder.create();
    }

    private void isValidTitleName(Dialog dialog) {
        if (dialog instanceof AlertDialog alertDialog) {
            Editable text = textInputEditText.getText();
            Objects.requireNonNull(text);
            String multi = text.toString();


            boolean result = !multi.isEmpty();


            alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(result);


            if (!notPrintErrorMessages.get()) {

                if (multi.isEmpty()) {
                    textInputLayout.setError(getString(R.string.name_not_valid));
                } else {
                    textInputLayout.setError(null);
                }

            } else {
                textInputLayout.setError(null);
            }
        }
    }

    private void removeKeyboards() {

        try {
            InputMethodManager imm = (InputMethodManager)
                    requireContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            if (imm != null) {
                imm.hideSoftInputFromWindow(textInputEditText.getWindowToken(), 0);
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }

    }

    @Override
    public void onDismiss(@NonNull DialogInterface dialog) {
        super.onDismiss(dialog);
        removeKeyboards();
    }

}
