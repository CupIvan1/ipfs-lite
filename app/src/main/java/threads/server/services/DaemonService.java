package threads.server.services;

import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Icon;
import android.net.ConnectivityManager;
import android.net.LinkProperties;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.os.IBinder;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.work.WorkManager;

import java.util.List;
import java.util.Objects;

import threads.lite.core.Server;
import threads.server.LogUtils;
import threads.server.MainActivity;
import threads.server.R;
import threads.server.core.DOCS;
import threads.server.work.AutonatWorker;

public class DaemonService extends Service {

    private static final String ACTION_STOP_SERVICE = "ACTION_STOP_SERVICE";
    private static final String ACTION_START_SERVICE = "ACTION_START_SERVICE";


    private static final String TAG = DaemonService.class.getSimpleName();

    private static final String DAEMON_CHANNEL_ID = "DAEMON_CHANNEL_ID";

    private static void createDaemonChannel(@NonNull Context context) {
        try {
            CharSequence name = context.getString(R.string.daemon_channel_name);
            String description = context.getString(R.string.daemon_channel_description);
            NotificationChannel mChannel = new NotificationChannel(DAEMON_CHANNEL_ID, name,
                    NotificationManager.IMPORTANCE_DEFAULT);

            mChannel.setDescription(description);

            NotificationManager notificationManager = (NotificationManager) context.getSystemService(
                    Context.NOTIFICATION_SERVICE);

            notificationManager.createNotificationChannel(mChannel);

        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    public static void start(@NonNull Context context) {
        try {
            Intent intent = new Intent(context, DaemonService.class);
            intent.setAction(ACTION_START_SERVICE);
            context.startForegroundService(intent);
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }


    private void registerNetworkCallback() {
        try {
            ConnectivityManager connectivityManager = (ConnectivityManager)
                    getSystemService(Context.CONNECTIVITY_SERVICE);
            connectivityManager.registerDefaultNetworkCallback(new ConnectivityManager.NetworkCallback() {
                @Override
                public void onAvailable(@NonNull Network network) {
                    try {
                        DOCS.getInstance(getApplicationContext()).updateNetwork();
                        AutonatWorker.autonat(getApplicationContext());
                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                    }
                }

                @Override
                public void onLost(@NonNull Network network) {
                    try {
                        DOCS.getInstance(getApplicationContext()).updateNetwork();
                        DOCS.getInstance(getApplicationContext())
                                .setReachability(DOCS.Reachability.UNKNOWN);

                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                    }
                }

                @Override
                public void onCapabilitiesChanged(@NonNull Network network,
                                                  @NonNull NetworkCapabilities networkCapabilities) {

                }

                @Override
                public void onLinkPropertiesChanged(@NonNull Network network,
                                                    @NonNull LinkProperties linkProperties) {

                }
            });

            if (threads.lite.cid.Network.isNetworkConnected(getApplicationContext())) {
                AutonatWorker.autonat(getApplicationContext());
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        if (Objects.equals(intent.getAction(), ACTION_START_SERVICE)) {
            buildNotification();
            registerNetworkCallback();
        } else if (Objects.equals(intent.getAction(), ACTION_STOP_SERVICE)) {
            try {
                stopForeground(STOP_FOREGROUND_REMOVE);
            } finally {
                stopSelf();
            }
        }

        return START_NOT_STICKY;
    }

    private void buildNotification() {
        try {
            createDaemonChannel(getApplicationContext());
            Notification.Builder builder = new Notification.Builder(
                    getApplicationContext(), DAEMON_CHANNEL_ID);

            Intent notifyIntent = new Intent(getApplicationContext(), MainActivity.class);
            int viewID = (int) System.currentTimeMillis();
            PendingIntent viewIntent = PendingIntent.getActivity(getApplicationContext(),
                    viewID, notifyIntent,
                    PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_IMMUTABLE);


            Intent stopIntent = new Intent(getApplicationContext(), DaemonService.class);
            stopIntent.setAction(ACTION_STOP_SERVICE);
            int requestID = (int) System.currentTimeMillis();
            PendingIntent stopPendingIntent = PendingIntent.getService(
                    getApplicationContext(), requestID, stopIntent,
                    PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_IMMUTABLE);

            Notification.Action action = new Notification.Action.Builder(
                    Icon.createWithResource(getApplicationContext(), R.drawable.pause),
                    getString(R.string.shutdown),
                    stopPendingIntent).build();

            builder.setSmallIcon(R.drawable.access_point_network);
            builder.addAction(action);
            builder.setContentTitle(getString(R.string.application_is_running));
            builder.setContentIntent(viewIntent);
            builder.setCategory(Notification.CATEGORY_SERVICE);
            builder.setUsesChronometer(true);
            builder.setOnlyAlertOnce(true);
            builder.setOngoing(true);

            Notification notification = builder.build();
            startForeground(TAG.hashCode(), notification);
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }


    /**
     * @noinspection CallToSystemExit
     */
    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            // cancel all works
            WorkManager.getInstance(getApplicationContext()).cancelAllWork();

            // closing app
            DOCS docs = DOCS.getInstance(getApplicationContext());
            Server server = docs.getServer();
            server.shutdown();
            removeFromResents();

            //noinspection CallToSystemExit
            System.exit(0);
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    private void removeFromResents() {
        try {
            ActivityManager am = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
            if (am != null) {
                List<ActivityManager.AppTask> tasks = am.getAppTasks();
                if (tasks != null && tasks.size() > 0) {
                    tasks.get(0).setExcludeFromRecents(true);
                }
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }
}
