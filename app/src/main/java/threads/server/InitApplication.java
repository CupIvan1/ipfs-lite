package threads.server;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;

import androidx.annotation.NonNull;

import com.google.android.material.color.DynamicColors;

import java.util.Objects;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import threads.lite.IPFS;
import threads.lite.Utils;
import threads.lite.core.Page;
import threads.lite.core.Server;
import threads.lite.mdns.MDNS;
import threads.server.core.DOCS;
import threads.server.core.events.EVENTS;
import threads.server.services.DaemonService;

public class InitApplication extends Application {


    private static final String APP_KEY = "LITE_KEY";
    private static final String TITLE_KEY = "TITLE_KEY";
    private static final String TAG = InitApplication.class.getSimpleName();

    private MDNS mdns;

    public static void setTitle(@NonNull Context context, @NonNull String title) {
        SharedPreferences sharedPref = context.getSharedPreferences(
                APP_KEY, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(TITLE_KEY, title);
        editor.apply();
    }

    @NonNull
    public static String getTitle(@NonNull Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences(
                APP_KEY, Context.MODE_PRIVATE);
        return Objects.requireNonNull(sharedPref.getString(TITLE_KEY,
                context.getString(R.string.homepage)));

    }


    @Override
    public void onCreate() {
        super.onCreate();

        DynamicColors.applyToActivitiesIfAvailable(this);

        registerService();

        // else case is handled in MainActivity
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.TIRAMISU) {
            DaemonService.start(getApplicationContext());
        }
    }


    private void registerService() {
        EVENTS events = EVENTS.getInstance(getApplicationContext());
        try {

            mdns = IPFS.mdns(getApplicationContext());

            IPFS ipfs = IPFS.getInstance(getApplicationContext());
            DOCS docs = DOCS.getInstance(getApplicationContext());


            ipfs.setRecordSupplier(() -> {
                try {
                    Page page = docs.getHomePage();
                    if (page != null) {
                        return ipfs.createSelfSignedIpnsRecord(page.value(), page.name());
                    }
                    return ipfs.createSelfSignedIpnsRecord(Utils.BYTES_EMPTY, null);
                } catch (Throwable throwable) {
                    throw new RuntimeException(throwable);
                }
            });


            Server server = docs.getServer();

            mdns.startService(server);


            ExecutorService executor = Executors.newSingleThreadExecutor();
            executor.execute(() -> docs.initPinsPage(
                    InitApplication.getTitle(getApplicationContext())));
        } catch (Throwable throwable) {
            events.fatal(throwable.getClass().getSimpleName() + throwable.getMessage());
            unRegisterService();
            LogUtils.error(TAG, throwable);
        }
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        unRegisterService();
    }

    private void unRegisterService() {
        try {
            if (mdns != null) {
                mdns.stop();
                mdns = null;
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }
}
