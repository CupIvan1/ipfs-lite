package threads.server.core.events;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public record Event(@PrimaryKey @NonNull @ColumnInfo(name = "identifier") String identifier,
                    @NonNull @ColumnInfo(name = "content") String content) {

    public static Event createEvent(@NonNull String identifier, @NonNull String content) {
        return new Event(identifier, content);
    }


    @Override
    @NonNull
    public String content() {
        return content;
    }


    @Override
    @NonNull
    public String identifier() {
        return identifier;
    }


}
