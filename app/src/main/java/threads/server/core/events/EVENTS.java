package threads.server.core.events;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.room.Room;

public class EVENTS {
    public static final String TITLE = "TITLE";
    public static final String OFFLINE = "OFFLINE";
    public static final String ONLINE = "ONLINE";
    public static final String DELETE = "DELETE";
    public static final String FATAL = "FATAL";
    public static final String ERROR = "FAILURE";
    public static final String WARNING = "WARNING";
    public static final String REACHABILITY = "REACHABILITY";
    public static final String CONNECTIONS = "CONNECTIONS";

    public static final String PERMISSION = "PERMISSION";
    private static volatile EVENTS INSTANCE = null;
    private final EventsDatabase eventsDatabase;

    private EVENTS(EventsDatabase eventsDatabase) {
        this.eventsDatabase = eventsDatabase;
    }

    @NonNull
    private static EVENTS createEvents(@NonNull EventsDatabase eventsDatabase) {
        return new EVENTS(eventsDatabase);
    }

    public static EVENTS getInstance(@NonNull Context context) {

        if (INSTANCE == null) {
            synchronized (EVENTS.class) {
                if (INSTANCE == null) {
                    EventsDatabase eventsDatabase =
                            Room.inMemoryDatabaseBuilder(context,
                                            EventsDatabase.class).
                                    allowMainThreadQueries().build();
                    INSTANCE = EVENTS.createEvents(eventsDatabase);
                }
            }
        }
        return INSTANCE;
    }

    @NonNull
    static Event createEvent(@NonNull String identifier, @NonNull String content) {

        return Event.createEvent(identifier, content);
    }

    public void permission(@NonNull String content) {
        storeEvent(createEvent(PERMISSION, content));
    }

    public void connections() {
        storeEvent(createEvent(CONNECTIONS, ""));
    }

    public void reachability(String reachability) {
        storeEvent(createEvent(REACHABILITY, reachability));
    }

    public void error(@NonNull String content) {
        storeEvent(createEvent(ERROR, content));
    }

    public void fatal(@NonNull String content) {
        storeEvent(createEvent(FATAL, content));
    }

    public void delete(@NonNull String content) {
        storeEvent(createEvent(DELETE, content));
    }

    public void online() {
        storeEvent(createEvent(ONLINE, ""));
    }

    public void offline() {
        storeEvent(createEvent(OFFLINE, ""));
    }

    public void title() {
        storeEvent(createEvent(TITLE, ""));
    }

    public void warning(@NonNull String content) {
        storeEvent(createEvent(WARNING, content));
    }

    @NonNull
    public EventsDatabase getEventsDatabase() {
        return eventsDatabase;
    }

    void storeEvent(@NonNull Event event) {

        eventsDatabase.eventDao().insertEvent(event);
    }


}
